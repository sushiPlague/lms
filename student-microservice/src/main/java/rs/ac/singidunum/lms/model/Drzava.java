package rs.ac.singidunum.lms.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import rs.ac.singidunum.lms.dto.DTO;
import rs.ac.singidunum.lms.dto.DrzavaDTO;
import rs.ac.singidunum.lms.dto.MestoDTO;

@Entity
public class Drzava implements GenericModel<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	@Column(nullable = false, unique = true)
	private String naziv;

	@OneToMany(mappedBy = "drzava")
	private Set<Mesto> mesta = new HashSet<Mesto>();

	public Drzava() {
		super();
	}

	public Drzava(Long id, String naziv, Set<Mesto> mesta) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.mesta = mesta;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Mesto> getMesta() {
		return mesta;
	}

	public void setMesta(Set<Mesto> mesta) {
		this.mesta = mesta;
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}

	@Override
	public DTO getDTO() {
		ArrayList<MestoDTO> mesta = (ArrayList<MestoDTO>) this.mesta.stream()
				.map(m -> new MestoDTO(m.getId(), m.getNaziv())).collect(Collectors.toList());

		return new DrzavaDTO(this.id, this.naziv, mesta);
	}

}
