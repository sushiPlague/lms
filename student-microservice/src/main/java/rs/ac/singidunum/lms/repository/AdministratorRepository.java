package rs.ac.singidunum.lms.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.model.Administrator;

@Repository
public interface AdministratorRepository extends CrudRepository<Administrator, Long> {
	
	public Optional<Administrator> findByKorisnickoIme(String korisnickoIme);
}
