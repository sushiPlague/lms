package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Mesto;

@Service
public class MestoService extends GenericService<Mesto, Long> {

}
