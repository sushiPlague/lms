package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Drzava;

@Service
public class DrzavaService extends GenericService<Drzava, Long> {

}
