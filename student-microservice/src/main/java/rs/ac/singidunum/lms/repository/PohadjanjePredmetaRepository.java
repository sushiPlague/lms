package rs.ac.singidunum.lms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.model.PohadjanjePredmeta;

@Repository
public interface PohadjanjePredmetaRepository extends CrudRepository<PohadjanjePredmeta, Long> {

}
