package rs.ac.singidunum.lms.dto;

import java.util.ArrayList;

public class StudentDTO extends DTO {

	private Long id;
	private String jmbg;
	private String ime;
	private AdresaDTO adresa;
	private StudentNaGodiniDTO studentNaGodini;
	private ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta = new ArrayList<PohadjanjePredmetaDTO>();
	private String korisnickoIme;
	private String lozinka;

	public StudentDTO() {
		super();
	}

	public StudentDTO(Long id, String jmbg, String ime) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa, StudentNaGodiniDTO studentNaGodini) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.studentNaGodini = studentNaGodini;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa,
			ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa, StudentNaGodiniDTO studentNaGodini,
			ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.studentNaGodini = studentNaGodini;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa, StudentNaGodiniDTO studentNaGodini,
			ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta, String korisnickoIme) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.studentNaGodini = studentNaGodini;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
		this.korisnickoIme = korisnickoIme;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa,
			ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta, String korisnickoIme) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
		this.korisnickoIme = korisnickoIme;
	}

	public StudentDTO(Long id, String jmbg, String ime, AdresaDTO adresa, StudentNaGodiniDTO studentNaGodini,
			ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.studentNaGodini = studentNaGodini;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public AdresaDTO getAdresa() {
		return adresa;
	}

	public void setAdresa(AdresaDTO adresa) {
		this.adresa = adresa;
	}

	public StudentNaGodiniDTO getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodiniDTO studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	public ArrayList<PohadjanjePredmetaDTO> getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

}
