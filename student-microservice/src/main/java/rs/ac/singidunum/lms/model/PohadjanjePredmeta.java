package rs.ac.singidunum.lms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import rs.ac.singidunum.lms.dto.DTO;

@Entity
public class PohadjanjePredmeta implements GenericModel<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private int konacnaOcena;

	@ManyToOne
	private Student student;

	// potrebno je dodati realizaciju predmeta iz odredjenog microservice-a

	public PohadjanjePredmeta() {
		super();
	}

	public PohadjanjePredmeta(Long id, int konacnaOcena, Student student) {
		super();
		this.id = id;
		this.konacnaOcena = konacnaOcena;
		this.student = student;
	}

	public int getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(int konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}

	@Override
	public DTO getDTO() {
		// TODO Auto-generated method stub
		return null;
	}

}
