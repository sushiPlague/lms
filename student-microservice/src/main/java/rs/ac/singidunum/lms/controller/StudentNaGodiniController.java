package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.StudentNaGodini;

@Controller
@RequestMapping(path = "/studenti-na-godini")
public class StudentNaGodiniController extends GenericController<StudentNaGodini, Long> {

}
