package rs.ac.singidunum.lms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.lms.dto.DTO;

@Entity
public class StudentNaGodini implements GenericModel<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date datumUpisa;

	@Column(unique = true, nullable = false)
	private String brojIndeksa;

	@OneToOne(mappedBy = "studentNaGodini")
	private Student student;

	// potrebno je dodati godinu studija itd iz univerzitet-microservice

	public StudentNaGodini() {
		super();
	}

	public StudentNaGodini(Long id, Date datumUpisa, String brojIndeksa, Student student) {
		super();
		this.id = id;
		this.datumUpisa = datumUpisa;
		this.brojIndeksa = brojIndeksa;
		this.student = student;
	}

	public Date getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(Date datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}

	@Override
	public DTO getDTO() {
		// TODO Auto-generated method stub
		return null;
	}

}
