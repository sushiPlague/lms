package rs.ac.singidunum.lms.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Nastavnik;
import rs.ac.singidunum.lms.repository.NastavnikRepository;
import rs.ac.singidunum.lms.utils.TokenUtils;

@Service
public class NastavnikService extends GenericService<Nastavnik, Long> {
	// crudService zna za crudRepository koji je genericki i ako zelimo neku od funkcionalnosti koju ima 
	// neka od clasa koja ga extenduje onda moramo da kastujemo
	
//	@Autowired
//	NastavnikRepository nastavnikRepository;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	public Optional<Nastavnik> findByKorisnickoIme(String korisnickoIme){
		// ako crudService autowireuje crudRepository jel moguce da se kastuje?
		return ((NastavnikRepository)this.repository).findByKorisnickoIme(korisnickoIme);
		
//		return nastavnikRepository.findByKorisnickoIme(korisnickoIme);
	}
	
	@Override
	public Nastavnik save(Nastavnik model) {
		String korisnickoIme = model.getKorisnickoIme();
		// provera da li se radi kreiranje ili izmena
		// kreiranje nema id
		if (model.getID() == null) {
			// znaci da bi se kreirao
			if(((NastavnikRepository)this.repository).findByKorisnickoIme(korisnickoIme).isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				return null;
			} else {
				// ne postoji korisnik sa tim korisnickim imenom
				return repository.save(model);
			}
		} else {
			// korisnik se menja
			Optional<Nastavnik> nastavnik = ((NastavnikRepository)this.repository).findByKorisnickoIme(korisnickoIme);
			if(nastavnik.isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				// dozvoljeno samo ako je to njegovo korisnickoIme
				if (nastavnik.get().getID() == model.getID()) {
					return repository.save(model);
				} else {
					// nije njegovo korisnicko ime
					return null;
				}
			} else {
				// korisnicko ime je jedinstveno
				return repository.save(model);
			}
		}
	}
	
	public Boolean doUsernamesMatch(String usernameToMatchWith, String token) {
		String username = tokenUtils.getUsername(token);
		if (usernameToMatchWith == username) {
			return true;
		} else {
			return false;
		}
	}
}
