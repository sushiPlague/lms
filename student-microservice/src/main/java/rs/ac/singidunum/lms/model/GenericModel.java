package rs.ac.singidunum.lms.model;

import java.io.Serializable;

import rs.ac.singidunum.lms.dto.DTO;

public interface GenericModel<ID extends Serializable> {
	public ID getID();
	public void setID(ID id);
	public DTO getDTO();
}
