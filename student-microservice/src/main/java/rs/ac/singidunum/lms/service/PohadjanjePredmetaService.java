package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.PohadjanjePredmeta;

@Service
public class PohadjanjePredmetaService extends GenericService<PohadjanjePredmeta, Long> {

}
