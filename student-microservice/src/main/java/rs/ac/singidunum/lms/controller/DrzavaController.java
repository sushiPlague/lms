package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Drzava;

@Controller
@RequestMapping(path = "/drzave")
public class DrzavaController extends GenericController<Drzava, Long> {

}
