package rs.ac.singidunum.lms.model;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.HashSet;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import rs.ac.singidunum.lms.dto.NaucnaOblastDTO;
import rs.ac.singidunum.lms.dto.ZvanjeDTO;

@Entity
public class NaucnaOblast implements GenericModel<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String naziv;

	@OneToMany(mappedBy = "naucnaOblast")
	private Set<Zvanje> zvanja = new HashSet<Zvanje>();

	public NaucnaOblast() {
		super();
	}

	public NaucnaOblast(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Zvanje> getZvanja() {
		return zvanja;
	}

	public void setZvanja(Set<Zvanje> zvanja) {
		this.zvanja = zvanja;
	}

	@Override
	public NaucnaOblastDTO getDTO() {
		ArrayList<ZvanjeDTO> zvanja = (ArrayList<ZvanjeDTO>) this.zvanja.stream()
				.map(z -> new ZvanjeDTO(z.getID(), z.getDatumIzbora(), z.getDatumPrestanka()))
				.collect(Collectors.toList());
		return new NaucnaOblastDTO(this.id, this.naziv, zvanja);
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}
}
