package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Student;

@Service
public class StudentService extends GenericService<Student, Long> {

}
