package rs.ac.singidunum.lms.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import rs.ac.singidunum.lms.dto.DTO;
import rs.ac.singidunum.lms.model.GenericModel;
import rs.ac.singidunum.lms.service.GenericService;

public abstract class GenericController<T extends GenericModel<ID>, ID extends Serializable> {

	@Autowired
	protected GenericService<T, ID> service;

	@GetMapping
	public ResponseEntity<ArrayList<DTO>> getAll() {
		ArrayList<DTO> t = new ArrayList<DTO>();
		Iterable<T> t1 = service.findAll();

		for (T t2 : t1) {
			t.add(t2.getDTO());
		}

		return new ResponseEntity<ArrayList<DTO>>(t, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<DTO> getOne(@PathVariable("id") ID id) {
		Optional<T> t = service.findOne(id);

		if (t.isPresent()) {
			return new ResponseEntity<DTO>(t.get().getDTO(), HttpStatus.OK);
		}

		return new ResponseEntity<DTO>(HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public ResponseEntity<DTO> create(@RequestBody T t) {
		try {
			service.save(t);
			return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<DTO>(HttpStatus.BAD_REQUEST);
	}

	@PutMapping("/{id}")
	public ResponseEntity<DTO> update(@RequestBody T t, @PathVariable("id") ID id) {
		Optional<T> t1 = service.findOne(id);

		if (t1.isPresent()) {
			t.setID(id);
			service.save(t);

			return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.CREATED);
		}

		return new ResponseEntity<DTO>(HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<DTO> delete(@PathVariable("id") ID id) {
		Optional<T> t1 = service.findOne(id);

		if (t1.isPresent()) {
			service.delete(id);

			return new ResponseEntity<DTO>(HttpStatus.OK);
		}

		return new ResponseEntity<DTO>(HttpStatus.BAD_REQUEST);
	}
}
