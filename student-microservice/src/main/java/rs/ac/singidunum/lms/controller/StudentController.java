package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Student;

@Controller
@RequestMapping(path = "/studenti")
public class StudentController extends GenericController<Student, Long> {

}
