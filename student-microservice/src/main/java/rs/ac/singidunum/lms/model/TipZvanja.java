package rs.ac.singidunum.lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import rs.ac.singidunum.lms.dto.TipZvanjaDTO;
import rs.ac.singidunum.lms.dto.ZvanjeDTO;

@Entity
public class TipZvanja implements GenericModel<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String naziv;

	@OneToOne(mappedBy = "tipZvanja")
	private Zvanje zvanje;

	public TipZvanja() {
		super();
	}

	public TipZvanja(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Zvanje getZvanje() {
		return zvanje;
	}

	public void setZvanje(Zvanje zvanje) {
		this.zvanje = zvanje;
	}

	@Override
	public TipZvanjaDTO getDTO() {
		ZvanjeDTO zvanje = new ZvanjeDTO(this.zvanje.getID(), this.zvanje.getDatumIzbora(),
				this.zvanje.getDatumPrestanka());
		return new TipZvanjaDTO(this.id, this.naziv, zvanje);
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}
}
