package rs.ac.singidunum.lms.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.lms.dto.NastavniciZvanjaDTO;
import rs.ac.singidunum.lms.dto.NastavnikDTO;
import rs.ac.singidunum.lms.dto.NaucnaOblastDTO;
import rs.ac.singidunum.lms.dto.TipZvanjaDTO;
import rs.ac.singidunum.lms.dto.ZvanjeDTO;

@Entity
public class Zvanje implements GenericModel<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date datumIzbora;

	@Temporal(TemporalType.DATE)
	private Date datumPrestanka;

	@OneToOne(optional = false)
	private TipZvanja tipZvanja;

	@ManyToOne(optional = false)
	private NaucnaOblast naucnaOblast;

	@OneToMany(mappedBy = "zvanje")
	private Set<NastavniciZvanja> nastavnici = new HashSet<NastavniciZvanja>();

	public Zvanje() {
		super();
	}

	public Zvanje(Long id, Date datumIzbora, Date datumPrestanka, TipZvanja tipZvanja, NaucnaOblast naucnaOblast) {
		super();
		this.id = id;
		this.datumIzbora = datumIzbora;
		this.datumPrestanka = datumPrestanka;
		this.tipZvanja = tipZvanja;
		this.naucnaOblast = naucnaOblast;
	}

	public Date getDatumIzbora() {
		return datumIzbora;
	}

	public void setDatumIzbora(Date datumIzbora) {
		this.datumIzbora = datumIzbora;
	}

	public Date getDatumPrestanka() {
		return datumPrestanka;
	}

	public void setDatumPrestanka(Date datumPrestanka) {
		this.datumPrestanka = datumPrestanka;
	}

	public TipZvanja getTipZvanja() {
		return tipZvanja;
	}

	public void setTipZvanja(TipZvanja tipZvanja) {
		this.tipZvanja = tipZvanja;
	}

	public NaucnaOblast getNaucnaOblast() {
		return naucnaOblast;
	}

	public void setNaucnaOblast(NaucnaOblast naucnaOblast) {
		this.naucnaOblast = naucnaOblast;
	}

	public Set<NastavniciZvanja> getNastavnici() {
		return this.nastavnici;
	}

	public void setNastavnici(Set<NastavniciZvanja> nastavnici) {
		this.nastavnici = nastavnici;
	}

	@Override
	public ZvanjeDTO getDTO() {
		TipZvanjaDTO tipZvanja = new TipZvanjaDTO(this.tipZvanja.getID(), this.tipZvanja.getNaziv());
		NaucnaOblastDTO naucnaOblast = new NaucnaOblastDTO(this.naucnaOblast.getID(), this.naucnaOblast.getNaziv());
		ArrayList<NastavniciZvanjaDTO> nastavnici = (ArrayList<NastavniciZvanjaDTO>) this.nastavnici.stream()
				.map(n -> new NastavniciZvanjaDTO(n.getID(), new NastavnikDTO(n.getNastavnik().getID(),
						n.getNastavnik().getIme(), n.getNastavnik().getPrezime(), n.getNastavnik().getKorisnickoIme(),
						n.getNastavnik().getLozinka(), n.getNastavnik().getBiografija(), n.getNastavnik().getJmbg())))
				.collect(Collectors.toList());

		return new ZvanjeDTO(this.id, this.datumIzbora, this.datumPrestanka, tipZvanja, naucnaOblast, nastavnici);
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}
}
