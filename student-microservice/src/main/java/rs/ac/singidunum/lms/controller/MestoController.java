package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Mesto;

@Controller
@RequestMapping(path = "/mesta")
public class MestoController extends GenericController<Mesto, Long> {

}
