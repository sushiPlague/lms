package rs.ac.singidunum.lms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.model.Drzava;

@Repository
public interface DrzavaRepository extends CrudRepository<Drzava, Long> {

}
