package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.PohadjanjePredmeta;

@Controller
@RequestMapping(path = "/pohadjanja")
public class PohadjanjePredmetaController extends GenericController<PohadjanjePredmeta, Long> {

}
