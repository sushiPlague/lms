package rs.ac.singidunum.lms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.model.Mesto;

@Repository
public interface MestoRepository extends CrudRepository<Mesto, Long> {

}
