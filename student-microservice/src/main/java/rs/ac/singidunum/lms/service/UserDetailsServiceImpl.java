package rs.ac.singidunum.lms.service;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Administrator;
import rs.ac.singidunum.lms.model.Nastavnik;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    NastavnikService nastavnikService;
    
    @Autowired
    AdministratorService administratorService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// Kako pristup nekim od ovih endpointa imaju nastavnici a nekim administratori ima dobavljanje za oba a nastavice se za pronadjenog
		Optional<Nastavnik> nastavnik = nastavnikService.findByKorisnickoIme(username);
		
		if(nastavnik.isPresent()) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_NASTAVNIK"));
			return new org.springframework.security.core.userdetails.User(nastavnik.get().getKorisnickoIme(), nastavnik.get().getLozinka(), grantedAuthorities);
		}
		
		Optional<Administrator> administrator = administratorService.findByKorisnickoIme(username);
		if(administrator.isPresent()) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			return new org.springframework.security.core.userdetails.User(administrator.get().getKorisnickoIme(), administrator.get().getLozinka(), grantedAuthorities);
		}
		
		
		return null;
	}
}
