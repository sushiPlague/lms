package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Adresa;

@Service
public class AdresaService extends GenericService<Adresa, Long> {

}
