package rs.ac.singidunum.lms.dto;

import java.util.Date;
import java.util.ArrayList;


public class ZvanjeDTO extends DTO {
	
	private Long id;
	private Date datumIzbora;
	private Date datumPrestanka;
	private TipZvanjaDTO tipZvanja;
	private NaucnaOblastDTO naucnaOblast;
	private ArrayList<NastavniciZvanjaDTO> nastavnici;
	
	public ZvanjeDTO() {
		super();
	}

	public ZvanjeDTO(Long id, Date datumIzbora, Date datumPrestanka, TipZvanjaDTO tipZvanja,
			NaucnaOblastDTO naucnaOblast, ArrayList<NastavniciZvanjaDTO> nastavnici) {
		super();
		this.id = id;
		this.datumIzbora = datumIzbora;
		this.datumPrestanka = datumPrestanka;
		this.tipZvanja = tipZvanja;
		this.naucnaOblast = naucnaOblast;
		this.nastavnici = nastavnici;
	}
	
	public ZvanjeDTO(Long id, Date datumIzbora, Date datumPrestanka) {
		this(id, datumIzbora, datumPrestanka, null, null, null);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumIzbora() {
		return datumIzbora;
	}

	public void setDatumIzbora(Date datumIzbora) {
		this.datumIzbora = datumIzbora;
	}

	public Date getDatumPrestanka() {
		return datumPrestanka;
	}

	public void setDatumPrestanka(Date datumPrestanka) {
		this.datumPrestanka = datumPrestanka;
	}

	public TipZvanjaDTO getTipZvanja() {
		return tipZvanja;
	}

	public void setTipZvanja(TipZvanjaDTO tipZvanja) {
		this.tipZvanja = tipZvanja;
	}

	public NaucnaOblastDTO getNaucnaOblast() {
		return naucnaOblast;
	}

	public void setNaucnaOblast(NaucnaOblastDTO naucnaOblast) {
		this.naucnaOblast = naucnaOblast;
	}

	public ArrayList<NastavniciZvanjaDTO> getNastavnici() {
		return nastavnici;
	}

	public void setNastavnik(ArrayList<NastavniciZvanjaDTO> nastavnici) {
		this.nastavnici = nastavnici;
	}
	
	
}
