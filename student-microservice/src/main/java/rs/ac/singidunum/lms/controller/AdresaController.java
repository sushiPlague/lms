package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Adresa;

@Controller
@RequestMapping(path = "/adrese")
public class AdresaController extends GenericController<Adresa, Long> {

}
