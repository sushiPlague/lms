package rs.ac.singidunum.lms.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.app.utils.TokenUtils;
import rs.ac.singidunum.lms.app.dto.TokenDTO;
import rs.ac.singidunum.lms.app.dto.KorisnikDTO;
import rs.ac.singidunum.lms.app.dto.NastavnikDTO;

import rs.ac.singidunum.lms.app.model.Nastavnik;
import rs.ac.singidunum.lms.app.dto.AdministratorDTO;
import rs.ac.singidunum.lms.app.model.Administrator;
import rs.ac.singidunum.lms.app.service.NastavnikService;
import rs.ac.singidunum.lms.app.model.Student;
import rs.ac.singidunum.lms.app.dto.StudentDTO;
import rs.ac.singidunum.lms.app.service.StudentService;
import rs.ac.singidunum.lms.app.service.AdministratorService;

@CrossOrigin(origins = "*")
@Controller
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private NastavnikService nastavnikService;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private AdministratorService administratorService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	
	@RequestMapping(path="/login2", method=RequestMethod.POST)
	public ResponseEntity<TokenDTO> logIn(@RequestBody KorisnikDTO korisnik) {
		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(korisnik.getKorisnickoIme(), korisnik.getLozinka());
			Authentication authentication = this.authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(korisnik.getKorisnickoIme());
			String jwt = this.tokenUtils.generateToken(userDetails);
			TokenDTO tokenDTO = new TokenDTO(jwt);
			
			return new ResponseEntity<TokenDTO>(tokenDTO, HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<TokenDTO>(HttpStatus.UNAUTHORIZED);
		}
		
		
	}
	// UVESTI PROVERU DA SE NE MOZE KREIRATI KORISNIK SA KORISNICKIM IMENOM KOJE VEC POSTOJI
	// UVESTI PROVERU DA AKO I IMA TOKEN DA LI MOZE DA PRISTUPI (NPR MENJANJE PODATAKA ADMINA OD STRANE ADMINA KOJI NIJE TAJ)
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/register/nastavnik", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> register(@RequestBody NastavnikDTO nastavnik) {
		// Novi korisnik se registruje kreiranjem instance korisnika
		// cija je lozinka enkodovana.
		Nastavnik noviNastavnik = new Nastavnik(null, nastavnik.getIme(), nastavnik.getPrezime(), nastavnik.getKorisnickoIme(),
				passwordEncoder.encode(nastavnik.getLozinka()), nastavnik.getBiografija(), nastavnik.getJmbg());
//		System.err.println(nastavnik.getIme());
		
		noviNastavnik = this.nastavnikService.save(noviNastavnik);
		if (noviNastavnik == null) {
			return new ResponseEntity<TokenDTO>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<TokenDTO>(HttpStatus.OK);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/register/student", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> register(@RequestBody StudentDTO student) {
		
		Student noviStudent = new Student(null, student.getIme(), student.getKorisnickoIme(),
				passwordEncoder.encode(student.getLozinka()), student.getJmbg());
		
		noviStudent = studentService.save(noviStudent);
		if (noviStudent == null) {
			return new ResponseEntity<TokenDTO>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<TokenDTO>(HttpStatus.OK);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/register/admin", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> register(@RequestBody AdministratorDTO administrator) {
		
		Administrator noviAdministrator = new Administrator(null, administrator.getKorisnickoIme(),
				passwordEncoder.encode(administrator.getLozinka()));
		
		noviAdministrator = administratorService.save(noviAdministrator);
		if (noviAdministrator == null) {
			return new ResponseEntity<TokenDTO>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<TokenDTO>(HttpStatus.OK);
	}
}
