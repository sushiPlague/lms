package rs.ac.singidunum.lms.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.app.service.AdministratorService;
import rs.ac.singidunum.lms.app.dto.AdministratorDTO;
import rs.ac.singidunum.lms.app.model.Administrator;

@Controller
@RequestMapping(path = "/administratori")
public class AdministratorController{
	@Autowired
	private AdministratorService service;
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<AdministratorDTO>> getAll(){
		ArrayList<AdministratorDTO> t = new ArrayList<AdministratorDTO>();
		Iterable<Administrator> t1 = service.findAll();
		for (Administrator t2: t1) {
			t.add(t2.getDTO());
		}
		return new ResponseEntity<ArrayList<AdministratorDTO>>(t, HttpStatus.OK);
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<AdministratorDTO> getOne(@PathVariable("id") Long id){
		Optional<Administrator> t = service.findOne(id);
		if (t.isPresent()) {
			return new ResponseEntity<AdministratorDTO>(t.get().getDTO(), HttpStatus.OK);
		}
		return new ResponseEntity<AdministratorDTO>(HttpStatus.NOT_FOUND);
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<AdministratorDTO> update(@RequestHeader("Authorization") String token, @RequestBody Administrator t, @PathVariable("id") Long id){
		Optional<Administrator> t1 = service.findOne(id);
		if (t1.isPresent()) {
			// provera da li administrator zeli da menja svoje podatke, tudje ne moze
			if (this.service.doUsernamesMatch(t1.get().getKorisnickoIme(), token)) {
				t.setId(id);
				Administrator updatedAdministrator = service.save(t);
				// provera da li se moze izmeniti
				if(updatedAdministrator == null) {
					return new ResponseEntity<AdministratorDTO>(HttpStatus.BAD_REQUEST);
				}
				return new ResponseEntity<AdministratorDTO>(t.getDTO(), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<AdministratorDTO>(HttpStatus.UNAUTHORIZED);
			}
		}
		return new ResponseEntity<AdministratorDTO>(HttpStatus.NOT_FOUND);
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AdministratorDTO> delete(@PathVariable("id") Long id){
		Optional<Administrator> t1 = service.findOne(id);
		if (t1.isPresent()) {
			service.delete(id);
			return new ResponseEntity<AdministratorDTO>(HttpStatus.OK);
		}
		return new ResponseEntity<AdministratorDTO>(HttpStatus.BAD_REQUEST);
	}
}
