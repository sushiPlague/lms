package rs.ac.singidunum.lms.app.dto;

public class PohadjanjePredmetaDTO extends DTO {

	private Long id;
	private int konacnaOcena;
	private StudentDTO student;

	public PohadjanjePredmetaDTO() {
		super();
	}

	public PohadjanjePredmetaDTO(Long id, int konacnaOcena) {
		super();
		this.id = id;
		this.konacnaOcena = konacnaOcena;
	}

	public PohadjanjePredmetaDTO(Long id, int konacnaOcena, StudentDTO student) {
		super();
		this.id = id;
		this.konacnaOcena = konacnaOcena;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(int konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

}
