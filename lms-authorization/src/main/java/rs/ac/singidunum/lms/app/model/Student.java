package rs.ac.singidunum.lms.app.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.lms.app.dto.AdresaDTO;
import rs.ac.singidunum.lms.app.dto.DTO;
import rs.ac.singidunum.lms.app.dto.DrzavaDTO;
import rs.ac.singidunum.lms.app.dto.MestoDTO;
import rs.ac.singidunum.lms.app.dto.PohadjanjePredmetaDTO;
import rs.ac.singidunum.lms.app.dto.StudentDTO;

@Entity
public class Student extends AbstractKorisnik implements GenericModel<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true, nullable = false)
	private String jmbg;

	@Lob
	@Column(nullable = false)
	private String ime;

	@OneToOne(optional = true)
	private Adresa adresa;

	@OneToOne(optional = true)
	private StudentNaGodini studentNaGodini;

	@OneToMany(mappedBy = "student")
	private Set<PohadjanjePredmeta> pohadjanjePredmeta = new HashSet<PohadjanjePredmeta>();

	@Column(nullable = false)
	private String korisnickoIme;

	@Column(nullable = false)
	private String lozinka;

	public Student() {
		super();
	}

	public Student(Long id, String jmbg, String ime, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}

	public Student(Long id, String jmbg, String ime, Adresa adresa, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}

	public Student(Long id, String jmbg, String ime, Adresa adresa, StudentNaGodini studentNaGodini,
			Set<PohadjanjePredmeta> pohadjanjePredmeta, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresa = adresa;
		this.studentNaGodini = studentNaGodini;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	public Set<PohadjanjePredmeta> getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(Set<PohadjanjePredmeta> pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}

	@Override
	public DTO getDTO() {

		ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta = (ArrayList<PohadjanjePredmetaDTO>) this.pohadjanjePredmeta
				.stream().map(p -> new PohadjanjePredmetaDTO(p.getID(), p.getKonacnaOcena()))
				.collect(Collectors.toList());

		return new StudentDTO(this.id, this.jmbg, this.ime,
				new AdresaDTO(this.getAdresa().getID(), this.getAdresa().getUlica(), this.getAdresa().getBroj(),
						new MestoDTO(this.getAdresa().getMesto().getId(), this.getAdresa().getMesto().getNaziv(),
								new DrzavaDTO(this.getAdresa().getMesto().getDrzava().getID(),
										this.getAdresa().getMesto().getDrzava().getNaziv()))),
				pohadjanjePredmeta, this.korisnickoIme);
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

}
