package rs.ac.singidunum.lms.app.model;

import java.io.Serializable;

import rs.ac.singidunum.lms.app.dto.DTO;

public interface GenericModel<ID extends Serializable> {
	public ID getID();
	public void setID(ID id);
	public DTO getDTO();
}
