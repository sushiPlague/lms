package rs.ac.singidunum.lms.app.dto;

import java.util.Date;

public class StudentNaGodiniDTO extends DTO {

	private Long id;
	private Date datumUpisa;
	private String brojIndeksa;
	private StudentDTO student;

	public StudentNaGodiniDTO() {
		super();
	}

	public StudentNaGodiniDTO(Long id, Date datumUpisa, String brojIndeksa) {
		super();
		this.id = id;
		this.datumUpisa = datumUpisa;
		this.brojIndeksa = brojIndeksa;
	}

	public StudentNaGodiniDTO(Long id, Date datumUpisa, String brojIndeksa, StudentDTO student) {
		super();
		this.id = id;
		this.datumUpisa = datumUpisa;
		this.brojIndeksa = brojIndeksa;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(Date datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

}
