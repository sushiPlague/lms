package rs.ac.singidunum.lms.app.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.app.model.Student;
import java.util.Optional;

import rs.ac.singidunum.lms.app.repository.StudentRepository;

@Service
public class StudentService extends CrudService<Student, Long> {
	public Optional<Student> findByKorisnickoIme(String korisnickoIme){
		return ((StudentRepository)this.repository).findByKorisnickoIme(korisnickoIme);
	}
	
	@Override
	public Student save(Student model) {
		String korisnickoIme = model.getKorisnickoIme();
		// provera da li se radi kreiranje ili izmena
		// kreiranje nema id
		if (model.getID() == null) {
			// znaci da bi se kreirao
			if(((StudentRepository)this.repository).findByKorisnickoIme(korisnickoIme).isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				return null;
			} else {
				// ne postoji korisnik sa tim korisnickim imenom
				return repository.save(model);
			}
		} else {
			// korisnik se menja
			Optional<Student> student = ((StudentRepository)this.repository).findByKorisnickoIme(korisnickoIme);
			if(student.isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				// dozvoljeno samo ako je to njegovo korisnickoIme
				if (student.get().getID() == model.getID()) {
					return repository.save(model);
				} else {
					// nije njegovo korisnicko ime
					return null;
				}
			} else {
				// korisnicko ime je jedinstveno
				return repository.save(model);
			}
		}
	}
}
