package rs.ac.singidunum.lms.app.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.app.model.Nastavnik;


@Repository
public interface NastavnikRepository extends CrudRepository<Nastavnik, Long> {
	public Optional<Nastavnik> findByKorisnickoIme(String korisnickoIme);
}
