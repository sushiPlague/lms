package rs.ac.singidunum.lms.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import rs.ac.singidunum.lms.app.dto.DTO;
import rs.ac.singidunum.lms.app.dto.DrzavaDTO;
import rs.ac.singidunum.lms.app.dto.MestoDTO;

@Entity
public class Mesto implements GenericModel<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	@Column(nullable = false)
	private String naziv;

	@ManyToOne(optional = false)
	private Drzava drzava;

	@OneToMany(mappedBy = "mesto")
	private Set<Adresa> adrese = new HashSet<>();

	public Mesto() {
		super();
	}

	public Mesto(Long id, String naziv, Drzava drzava) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.drzava = drzava;
	}

	public Mesto(Long id, String naziv, Drzava drzava, Set<Adresa> adrese) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.drzava = drzava;
		this.adrese = adrese;
	}

	public Drzava getDrzava() {
		return drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}

	public Set<Adresa> getAdrese() {
		return adrese;
	}

	public void setAdrese(Set<Adresa> adrese) {
		this.adrese = adrese;
	}

	@Override
	public DTO getDTO() {
		return new MestoDTO(this.id, this.naziv, new DrzavaDTO(this.getDrzava().getID(), this.getDrzava().getNaziv()));
	}

}
