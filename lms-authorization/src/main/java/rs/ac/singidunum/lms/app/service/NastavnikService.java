package rs.ac.singidunum.lms.app.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.app.model.Nastavnik;
import rs.ac.singidunum.lms.app.repository.NastavnikRepository;


@Service
public class NastavnikService extends CrudService<Nastavnik, Long> {
	// crudService zna za crudRepository koji je genericki i ako zelimo neku od funkcionalnosti koju ima 
	// neka od clasa koja ga extenduje onda moramo da kastujemo
	
	
	public Optional<Nastavnik> findByKorisnickoIme(String korisnickoIme){
		// ako crudService autowireuje crudRepository jel moguce da se kastuje?
		return ((NastavnikRepository)this.repository).findByKorisnickoIme(korisnickoIme);
		
//		return nastavnikRepository.findByKorisnickoIme(korisnickoIme);
	}
	
	@Override
	public Nastavnik save(Nastavnik model) {
		String korisnickoIme = model.getKorisnickoIme();
		// provera da li se radi kreiranje ili izmena
		// kreiranje nema id
		if (model.getId() == null) {
			// znaci da bi se kreirao
			if(((NastavnikRepository)this.repository).findByKorisnickoIme(korisnickoIme).isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				return null;
			} else {
				// ne postoji korisnik sa tim korisnickim imenom
				return repository.save(model);
			}
		} else {
			// korisnik se menja
			Optional<Nastavnik> nastavnik = ((NastavnikRepository)this.repository).findByKorisnickoIme(korisnickoIme);
			if(nastavnik.isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				// dozvoljeno samo ako je to njegovo korisnickoIme
				if (nastavnik.get().getId() == model.getId()) {
					return repository.save(model);
				} else {
					// nije njegovo korisnicko ime
					return null;
				}
			} else {
				// korisnicko ime je jedinstveno
				return repository.save(model);
			}
		}
	}
}
