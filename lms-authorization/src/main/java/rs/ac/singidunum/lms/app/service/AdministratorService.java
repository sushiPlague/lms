package rs.ac.singidunum.lms.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.app.model.Administrator;
import rs.ac.singidunum.lms.app.repository.AdministratorRepository;
import rs.ac.singidunum.lms.app.utils.TokenUtils;


@Service
public class AdministratorService extends CrudService<Administrator, Long> {
	
	@Autowired
	private TokenUtils tokenUtils;
	
	public Optional<Administrator> findByKorisnickoIme(String korisnickoIme){
		return ((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme);
	}
	
	public Boolean doUsernamesMatch(String usernameToMatchWith, String token) {
		String username = tokenUtils.getUsername(token);
		if (usernameToMatchWith == username) {
			return true;
		} else {
			return false;
		}
	}
	@Override
	public Administrator save(Administrator model) {
		String korisnickoIme = model.getKorisnickoIme();
		// provera da li se radi kreiranje ili izmena
		// kreiranje nema id
		if (model.getId() == null) {
			// znaci da bi se kreirao
			if(((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme).isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				return null;
			} else {
				// ne postoji korisnik sa tim korisnickim imenom
				return repository.save(model);
			}
		} else {
			// korisnik se menja
			Optional<Administrator> administrator = ((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme);
			if(administrator.isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				// dozvoljeno samo ako je to njegovo korisnickoIme
				if (administrator.get().getId() == model.getId()) {
					return repository.save(model);
				} else {
					// nije njegovo korisnicko ime
					return null;
				}
			} else {
				// korisnicko ime je jedinstveno
				return repository.save(model);
			}
		}
	}
}
