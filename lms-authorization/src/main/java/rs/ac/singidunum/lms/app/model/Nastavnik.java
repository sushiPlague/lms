package rs.ac.singidunum.lms.app.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Nastavnik extends AbstractKorisnik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String ime;
	
	@Column(nullable = false)
	private String prezime;
	
	@Column(nullable = false)
	private String korisnickoIme;
	
	@Column(nullable = false)
	private String lozinka;
	
	// nullable je default true
	@Column
	private String biografija;
	
	@Column
	private Long jmbg;
	
	// referenca na nastavnika se u tabeli Zvanje nalazi na nastavnik polju
	@OneToMany(mappedBy = "nastavnik")
	private Set<NastavniciZvanja> zvanja = new HashSet<NastavniciZvanja>();

	public Nastavnik() {
		super();
	}
	// posto se referenca na nastavnika nalazi u Zvanje onda ne treba da u konstruktor dajes i zvanja jer nastavnik tabela ih nema
	public Nastavnik(Long id, String ime, String prezime, String korisnickoIme, String lozinka, String biografija, Long jmbg) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.biografija = biografija;
		this.jmbg = jmbg;
	}

	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}
	
	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	
	public String getBiografija() {
		return biografija;
	}

	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}

	public Long getJmbg() {
		return jmbg;
	}

	public void setJmbg(Long jmbg) {
		this.jmbg = jmbg;
	}

	
	public Set<NastavniciZvanja> getZvanja() {
		return zvanja;
	}
	public void setZvanja(Set<NastavniciZvanja> zvanja) {
		this.zvanja = zvanja;
	}
	
}
