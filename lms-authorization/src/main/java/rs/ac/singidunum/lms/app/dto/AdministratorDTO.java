package rs.ac.singidunum.lms.app.dto;

public class AdministratorDTO {
	private Long id;
	private String korisnickoIme;
	private String lozinka;
	
	public AdministratorDTO() {
		super();
	}
	public AdministratorDTO(Long id, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		
		this.lozinka = lozinka;
	}
	public AdministratorDTO(Long id, String korisnickoIme) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	
	
	
	
}
