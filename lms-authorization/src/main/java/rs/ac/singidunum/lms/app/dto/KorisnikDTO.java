package rs.ac.singidunum.lms.app.dto;

public class KorisnikDTO {
	
	private String korisnickoIme;
	private String lozinka;
	
	public KorisnikDTO() {
		super();
	}
	
	public KorisnikDTO(String korisnickoIme, String lozinka) {
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	
	
}
