package rs.ac.singidunum.lms.app.dto;

import java.util.ArrayList;


public class NastavnikDTO {
	
	private Long id;
	private String ime;
	private String prezime;
	private String korisnickoIme;
	private String lozinka;
	private String biografija;
	private Long jmbg;
	private ArrayList<NastavniciZvanjaDTO> zvanja = new ArrayList<NastavniciZvanjaDTO>();
	
	public NastavnikDTO() {
		super();
	}


	public NastavnikDTO(Long id, String ime, String prezime, String korisnickoIme, String lozinka, String biografija, Long jmbg, ArrayList<NastavniciZvanjaDTO> zvanja) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.biografija = biografija;
		this.jmbg = jmbg;
		this.zvanja = zvanja;
	}
	
	public NastavnikDTO(Long id, String ime, String prezime, String korisnickoIme, String lozinka, String biografija, Long jmbg) {
		this(id, ime, prezime, korisnickoIme, lozinka, biografija, jmbg, new ArrayList<NastavniciZvanjaDTO>());
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getBiografija() {
		return biografija;
	}

	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}

	public Long getJmbg() {
		return jmbg;
	}

	public void setJmbg(Long jmbg) {
		this.jmbg = jmbg;
	}

	public ArrayList<NastavniciZvanjaDTO> getZvanja() {
		return zvanja;
	}

	public void setZvanja(ArrayList<NastavniciZvanjaDTO> zvanja) {
		this.zvanja = zvanja;
	}
	
	
}
