package rs.ac.singidunum.lms.app.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.lms.app.model.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {
	public Optional<Student> findByKorisnickoIme(String korisnickoIme);
}
