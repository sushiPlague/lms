package rs.ac.singidunum.lms.app.service;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.app.model.Administrator;
import rs.ac.singidunum.lms.app.model.Nastavnik;
import rs.ac.singidunum.lms.app.model.Student;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    NastavnikService nastavnikService;
    
    @Autowired
    AdministratorService administratorService;
    
    @Autowired
    StudentService studentService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

//		Optional<Nastavnik> nastavnik = nastavnikService.findByKorisnickoIme(username);
//		if(nastavnik.isPresent()) {
//			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
//			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_NASTAVNIK"));
//			return new org.springframework.security.core.userdetails.User(nastavnik.get().getKorisnickoIme(), nastavnik.get().getLozinka(), grantedAuthorities);
//		}
		
//		Optional<Student> student = studentService.findByKorisnickoIme(username);
//		if(student.isPresent()) {
//			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
//			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_STUDENT"));
//			return new org.springframework.security.core.userdetails.User(student.get().getKorisnickoIme(), student.get().getLozinka(), grantedAuthorities);
//		}
		
		Optional<Administrator> administrator = administratorService.findByKorisnickoIme(username);
		if(administrator.isPresent()) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			return new org.springframework.security.core.userdetails.User(administrator.get().getKorisnickoIme(), administrator.get().getLozinka(), grantedAuthorities);
		}
		
		
		return null;
	}
}
