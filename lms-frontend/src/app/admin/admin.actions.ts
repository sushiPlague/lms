import { Action } from '@ngrx/store';

import { Admin } from '../auth/user models/admin.model';
import { Teacher } from '../auth/user models/teacher.model';
import { Student } from '../auth/user models/student.model';   

export const GET_ADMINS = '[Admin] Get Admins';
export const GET_TEACHERS = '[Admin] Get Teachers';
export const GET_STUDENTS = '[Admin] Get Students';
export const SET_ADMINS = '[Admin] Set Admins';
export const SET_TEACHERS = '[Admin] Set Teachers';
export const SET_STUDENTS = '[Admin] Set Students';

export class GetAdmins implements Action {
    readonly type = GET_ADMINS;

    public constructor(private payload: string){};
}

export class GetTeachers implements Action {
    readonly type = GET_TEACHERS;
}

export class GetStudents implements Action {
    readonly type = GET_STUDENTS;
}

export class SetAdmins implements Action {
    readonly type = SET_ADMINS;

    constructor(private payload: Admin[]){};
}

export class SetTeachers implements Action {
    readonly type = SET_TEACHERS;

    constructor(private payload: Teacher[]){};
}

export class SetStudents implements Action {
    readonly type = SET_STUDENTS;

    constructor(private payload: Student[]){};
}