import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { TeachersComponent } from './user-administration/teachers/teachers.component';
import { StudentsComponent } from './user-administration/students/students.component';
import { AdminsComponent } from './user-administration/admins/admins.component';
import { UserAdministrationComponent } from './user-administration/user-administration.component';
import { AdminFormComponent } from './user-administration/admin-form/admin-form.component';
import { TeachersFormComponent } from './user-administration/teachers-form/teachers-form.component';
import { StudentsFormComponent } from './user-administration/students-form/students-form.component';
import { adminReducer } from './admin.reducer';

@NgModule({
    declarations: [
        AdminComponent,
        TeachersComponent,
        StudentsComponent,
        AdminsComponent,
        UserAdministrationComponent,
        AdminFormComponent,
        TeachersFormComponent,
        StudentsFormComponent
    ],
    imports: [
        AdminRoutingModule,
        MaterialModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        StoreModule.forFeature("admin", adminReducer)
    ],
    exports: [
        AdminComponent
    ]
})
export class AdminModule { }