import { createFeatureSelector, createSelector } from '@ngrx/store';

import { Admin } from '../auth/user models/admin.model';
import { Student } from '../auth/user models/student.model';
import { Teacher } from '../auth/user models/teacher.model';

import * as fromRoot from '../app.reducer';

interface AdminState {
    admins: Admin[],
    students: Student[],
    teachers: Teacher[]
};


const initialState: AdminState = {
    admins: [],
    students: [],
    teachers: []
};

export interface State extends fromRoot.State {
    admin: AdminState
}

export function adminReducer(state = initialState, action){
    return state;
}

const getAdminState = createFeatureSelector<AdminState>('admin');
export const getAdmins = createSelector(getAdminState, (state: AdminState) => {return state.admins});
export const getStudents = createSelector(getAdminState, (state: AdminState) => {return state.students});
export const getTeachers = createSelector(getAdminState, (state: AdminState) => {return state.teachers});