import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';    

import * as Admin from './admin.actions';
import { of } from 'rxjs';


@Injectable()
export class AdminEffects{

    @Effect()
    admins$ = this.actions$.pipe(
        ofType(Admin.GET_ADMINS),
        switchMap((admin: Admin.GetAdmins)=>{
            return this.http.get('http://localhost:8080/api/auth/administratori',
            {
                headers: new HttpHeaders({'Authorization': })
            }
            )
            .pipe(
                map((resData)=>{
                    console.log(resData);
                    // return new Admin.SetAdmins()
                }),
                catchError((error)=>{
                    return of();
                })
            )
        })
    )

    public constructor(
        private actions$: Actions,
        private http: HttpClient
        ){};
}