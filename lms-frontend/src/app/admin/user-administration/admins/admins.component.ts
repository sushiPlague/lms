import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store'; 
import { map, take } from 'rxjs/operators';
// import { MatTableDataSource } from '@angular/material/table';

import { Admin } from 'src/app/auth/user models/admin.model';
// konvencija kad se nesto dobavlja iz reducera, da ide * i as from
import * as fromAdmin from '../../admin.reducer';
import * as adminActions from '../../admin.actions';
import * as fromRoot from '../../../app.reducer';
import { StartLoading } from '../../../shared/ui.actions';
// konvencija kad se nesto dobavlja iz action, da ide samo velikim slovima naziv
import * as UI from 'src/app/shared/ui.actions';
import { UIService } from 'src/app/shared/ui.service';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit, AfterViewInit {
  displayedColumns = ['id', 'korisnickoIme'];
  dataSource = new MatTableDataSource<Admin>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator
  // konvencija za varijable upravljane ng reduxom
  isLoading$: Observable<any>;
  // ZA LOADING CE SE STAVLJATI U SERVISU

  admins: Admin[] = [
    {id: 1, korisnickoIme: "admin1"},
    {id: 2, korisnickoIme: "admin2"}
  ];

  constructor(
    private store: Store<fromAdmin.State>
    ) { }

  ngOnInit(): void {
    this.isLoading$ = this.store.select(fromRoot.getIsLoading);
    this.getAdmins();
    this.store.select(fromAdmin.getAdmins).subscribe((admins)=>{
      this.dataSource.data = admins;
    });    
  }

  getAdmins(){
    // ako admin state nije dobavio pre admine
    if (this.store.select(fromAdmin.getAdmins).pipe(take(1)) == null){
      this.store.dispatch(new StartLoading());
      const token = this.store.select(fromRoot.getToken).pipe(take(1));
      this.store.dispatch(new adminActions.GetAdmins());
    }
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
