import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromRoot from '../app.reducer';
import * as Auth from '../auth/auth.actions';



@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  // ako postavljas atribut sa inicijalnom vrednosti onda je tip atributa tip inicijalne vrednosti,
  // ako ne postavljas onda treba da ga specificiras
  role$: Observable<any>;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit(): void {
    // da li treba iz root ili iz auth
    this.role$ = this.store.select(fromRoot.getIsAuthAs);
    console.log(this.role$);
  }

  logout(){
    this.store.dispatch(new Auth.SetUnauthenticated());
  }

  

}
