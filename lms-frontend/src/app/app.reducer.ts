import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUI from 'src/app/shared/ui.reducer';
import * as fromAuth from 'src/app/auth/auth.reducer';

// GLAVNI STATE KOJI SADRZI MANJE
export interface State {
    ui: fromUI.State;
    auth: fromAuth.State
}
// GLAVNI "REDUCER" KOJI SADRZI MANJE
export const reducers: ActionReducerMap<State> = {
    ui: fromUI.uiReducer,
    auth: fromAuth.authReducer
};
// SELECTOR SLUZI DA DOBAVI STATE
// feature selector sluzi za dobavljanje state-ova od "subreducera", tip je state sa kojim je podreducer i prima naziv podreducera
export const getUiState = createFeatureSelector<fromUI.State>('ui');
// selector sluzi da dobavimo polje state-a, prosledi se state iz kojeg treba nesto da radi i sta treba da radi
export const getIsLoading = createSelector(getUiState, fromUI.getIsLoading);

export const getAuthState = createFeatureSelector<fromAuth.State>('auth');
export const getIsAuthAs = createSelector(getAuthState, fromAuth.getIsAuthAs);
export const getToken = createSelector(getAuthState, fromAuth.getToken);
export const getIsLoginFail = createSelector(getAuthState, fromAuth.getIsLoginFail);