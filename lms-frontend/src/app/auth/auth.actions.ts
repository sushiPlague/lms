import { Action } from '@ngrx/store';

export const LOGIN_START = '[Auth] Login Start';
export const LOGIN_FAIL = '[Auth] Login Fail';
export const SET_AUTHENTICATED = '[Auth] Set Authenticated';
export const SET_UNAUTHENTICATED = '[Auth] Set Unauthenticated';
// DODATI I ZA TOKEN

interface AuthPayload {
    role: string,
    token: string
};

interface LoginInfo {
    korisnickoIme: string,
    lozinka: string
}

export class SetAuthenticated implements Action {
    readonly type = SET_AUTHENTICATED;

    constructor(public payload: AuthPayload){}
};

export class SetUnauthenticated implements Action {
    readonly type = SET_UNAUTHENTICATED;
}

export class LoginStart implements Action {
    readonly type = LOGIN_START;

    constructor(public payload: LoginInfo){}
}

export class LoginFail implements Action {
    readonly type = LOGIN_FAIL;
}


export type AuthActions = SetAuthenticated | SetUnauthenticated | LoginStart | LoginFail;