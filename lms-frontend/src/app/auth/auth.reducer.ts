import { AuthActions, SET_AUTHENTICATED, SET_UNAUTHENTICATED, LOGIN_FAIL} from './auth.actions';

export interface State {
    isAuthenticatedAs: string,
    token: string,
    authFail: boolean,
};

const initualState: State = {
    isAuthenticatedAs: 'ROLE_VISITOR',
    token: '',
    authFail: false
};

export function authReducer(state = initualState, action: AuthActions){
    switch(action.type){
        case SET_AUTHENTICATED:
            return {
                isAuthenticatedAs: action.payload.role,
                token: action.payload.token,
                authFail: false
            }
        case SET_UNAUTHENTICATED:
            return {
                ...state,
                isAuthenticatedAs: 'ROLE_VISITOR',
                token: ''
            }
        case LOGIN_FAIL:
            return {
                ...state,
                authFail: true
            }
        default:
            return state;
    }
}

export const getIsAuthAs = (state: State) => {return state.isAuthenticatedAs};
export const getToken = (state: State) => {return state.token};
export const getIsLoginFail = (state: State) => {return state.authFail};