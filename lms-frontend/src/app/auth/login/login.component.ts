import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as Auth from '../auth.actions';
import * as fromRoot from '../../app.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginFail$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit(): void {
    this.loginFail$ = this.store.select(fromRoot.getIsLoginFail);
  }

  onSubmit(form: NgForm){
    console.log(form.value);
    const loginInfo = {korisnickoIme: form.value.username, lozinka: form.value.password};
    console.log(loginInfo);
    this.store.dispatch(new Auth.LoginStart(loginInfo));
  }

}
