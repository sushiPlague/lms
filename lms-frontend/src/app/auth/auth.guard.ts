import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { AuthService } from './auth.service';
import * as fromRoot from 'src/app/app.reducer';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
  isAuthAs$: Observable<string | void>;

  constructor(private authService: AuthService, private router: Router, private store: Store<fromRoot.State>){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.isAuthAs$ = this.store.select(fromRoot.getIsAuthAs).pipe(take(1));
    if (this.isAuthAs$ != null && this.isAuthAs$ == next.data['role']){
      return true;
    } else {
      this.router.navigate(['/login']);
    }
  }
  // for lazy loading
  canLoad(
    route: Route): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // SAMO DOK SE PRAVI
    // if (this.isAuthAs$ != null && this.isAuthAs$ == route.data['role']){
    //   return true;
    // } else {
    //   this.router.navigate(['/login']);
    // }
    return true;
  }
  
}
