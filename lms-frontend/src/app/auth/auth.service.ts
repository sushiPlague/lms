import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Admin } from './user models/admin.model';
import { Teacher } from './user models/teacher.model';
import { Student } from './user models/student.model';   
import { AuthUser } from './user models/auth-user.model';
import * as fromRoot from '../app.reducer';
import * as Auth from './auth.actions';

interface AuthStatus{
    loggedIn: boolean;
    role: string;
}
 
@Injectable()
export class AuthService {
    // premestiti potrebne u state
    private user: Admin | Teacher | Student;
    private userToken: string;
    private userRole: string;
    private expirationDate: Date;

    authChange = new Subject<AuthStatus>();

    constructor(private router: Router, private store: Store<fromRoot.State>){

    }

    login(user: AuthUser){
        // vrsi se slanje zahteva na server i vraca se token i podaci o useru
        // token se parsira
        this.user = {korisnickoIme: "admin"};
        this.userToken = "token";
        // iz tokena
        this.userRole = "ROLE_ADMIN";
        this.expirationDate = new Date();
        const array = ["a", "b"];
        // this.authChange.next({loggedIn: true, role: this.userRole.slice()});

        this.store.dispatch(new Auth.LoginStart({korisnickoIme: "username", lozinka: "password"}));
        this.navigate(this.userRole);
    }

    registerAdmin(admin: Admin) {

    }

    registerTeacher(teacher: Teacher){

    }
    
    registerStudent(student: Student){

    }

    logout(){
        this.user = null;
        this.userToken = null;
        this.userRole = null;
        // this.authChange.next({loggedIn: false, role: null});
        this.store.dispatch(new Auth.SetUnauthenticated());
        this.router.navigate(['']);
    }

    getUser(){
        return { ...this.user }
    }

    getRole(){
        return this.userRole.slice();
    }

    isAuth(){
        return this.user != null;
    }

    navigate(user_role: string){
        switch(user_role){
            case "ROLE_ADMIN":
                this.router.navigate(['/admin']);
                return;
            case "ROLE_TEACHER":
                this.router.navigate(['/teacher']);
                return;
            case "ROLE_STUDENT":
                this.router.navigate(['/student']);
                return;
            // case "ROLE_VISITOR":
            //     this.router.navigate(['/']);
            //     return;
            default: 
                this.router.navigate(['/error_page']);
        }
    }
}