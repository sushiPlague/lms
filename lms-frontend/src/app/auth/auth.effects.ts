import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects'; 
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs'; 
import { Router } from '@angular/router';

import * as Auth from './auth.actions';   

@Injectable()
export class AuthEffects{

    @Effect()
    authLogin$ = this.actions$.pipe(
        ofType(Auth.LOGIN_START),
        switchMap((authData: Auth.LoginStart) => {
            return this.http.post("http://localhost:8080/api/auth/login2",
                JSON.stringify({
                    "korisnickoIme": authData.payload.korisnickoIme,
                    "lozinka": authData.payload.lozinka
                }),
                {
                    headers: new HttpHeaders({'Content-Type': 'application/json'})
                }
            )
            .pipe(
                map(resData => {
                    const role = JSON.parse(atob(resData['token'].split('.')[1]))['roles'][0]["authority"];
                    const token = resData['token'];
                    this.navigateUser(role);
                    return new Auth.SetAuthenticated({role: role, token: token});
                }),
                catchError(errorRes => {
                    return of(new Auth.LoginFail());
                })
            )
        })
    );
    
    // @Effect({dispatch: false})
    // authSuccess = this.actions$.pipe(
    //     ofType(Auth.SET_AUTHENTICATED), 
    //     tap(()=>{
    //         this.router.navigate(['/']);
    //     })
    //     )

    
    navigateUser(role: string){
        switch(role){
            case "ROLE_ADMIN":
                this.router.navigate(['/admin']);
                break;
            case "ROLE_STUDENT": 
                this.router.navigate(['/student']);
                break;
            case "ROLE_NASTAVNIK": 
                this.router.navigate(['/teacher']);
                break;
        }
    }

    constructor(private actions$: Actions, 
        private http: HttpClient,
        private router: Router
        ){};
}