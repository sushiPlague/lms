package rs.ac.singidunum.lms.repository;

import java.util.Date;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import rs.ac.singidunum.lms.model.Kabinet;
import rs.ac.singidunum.lms.model.Nastavnik;
import rs.ac.singidunum.lms.model.Predmet;
import rs.ac.singidunum.lms.model.RasporedPredavanja;

@Repository
public interface RasporedPredavanjaRepository extends CrudRepository<RasporedPredavanja, Long> {
	
//	@Lock(LockModeType.PESSIMISTIC_WRITE)
//	@Query("INSERT INTO RasporedPredavanja (id, vreme_pocetka, vreme_zavrsetka, kabinet, nastavnik, predmet) values (id=:id, vreme_pocetka=:vreme_pocetka, vreme_zavrsetka=:vreme_zavrsetka, kabinet=:kabinet, nastavnik=:nastavnik, predmet=:predmet)")
//	public RasporedPredavanja saveOne(Long id, Date vreme_pocetka, Date vreme_zavrsetka, Kabinet kabinet, Nastavnik nastavnik, Predmet predmet);
}
