package rs.ac.singidunum.lms.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Administrator;
import rs.ac.singidunum.lms.repository.AdministratorRepository;

@Service
public class AdministratorService extends CrudService<Administrator, Long> {
	
	public Optional<Administrator> findByKorisnickoIme(String korisnickoIme){
		return ((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme);
	}
	
}
