package rs.ac.singidunum.lms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import rs.ac.singidunum.lms.dto.KabinetDTO;
import rs.ac.singidunum.lms.dto.NastavnikDTO;
import rs.ac.singidunum.lms.dto.PredmetDTO;
import rs.ac.singidunum.lms.dto.RasporedPredavanjaDTO;

@Entity
public class RasporedPredavanja {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Date vremePocetka;
	
	@Column(nullable = false)
	private Date vremeZavrsetka;
	
	@ManyToOne(optional = false)
	private Kabinet kabinet;
	
	@ManyToOne(optional = false)
	private Nastavnik nastavnik;
	
	@ManyToOne(optional = false)
	private Predmet predmet;

	public RasporedPredavanja() {
		super();
	}

	public RasporedPredavanja(Long id, Date vremePocetka, Date vremeZavrsetka, Kabinet kabinet, Nastavnik nastavnik,
			Predmet predmet) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.kabinet = kabinet;
		this.nastavnik = nastavnik;
		this.predmet = predmet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(Date vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public Date getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(Date vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public Kabinet getKabinet() {
		return kabinet;
	}

	public void setKabinet(Kabinet kabinet) {
		this.kabinet = kabinet;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public Predmet getPredmet() {
		return predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
	
	public RasporedPredavanjaDTO getDTO() {
		NastavnikDTO nastavnik = new NastavnikDTO(this.nastavnik.getId(), this.nastavnik.getIme(), this.nastavnik.getPrezime(), this.nastavnik.getKorisnickoIme(), this.nastavnik.getLozinka(), this.nastavnik.getBiografija(), this.nastavnik.getJmbg());
		PredmetDTO predmet = new PredmetDTO(this.predmet.getNaziv());
	    KabinetDTO kabinet = new KabinetDTO(this.kabinet.getId(), this.kabinet.getNaziv());
	    
	    return new RasporedPredavanjaDTO(this.id, this.vremePocetka, this.vremeZavrsetka, kabinet, nastavnik, predmet);
	}
	
}
