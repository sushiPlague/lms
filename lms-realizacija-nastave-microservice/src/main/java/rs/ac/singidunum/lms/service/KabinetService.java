package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Kabinet;

@Service
public class KabinetService extends CrudService<Kabinet, Long> {

}
