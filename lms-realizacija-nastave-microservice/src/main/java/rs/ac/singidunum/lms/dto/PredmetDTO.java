package rs.ac.singidunum.lms.dto;

public class PredmetDTO {
	private String naziv;
	
	public PredmetDTO() {
		super();
	}
	
	public PredmetDTO(String naziv) {
		this.naziv = naziv;
	}
	
	public String getNaziv() {
		return this.naziv;
	}
	
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
