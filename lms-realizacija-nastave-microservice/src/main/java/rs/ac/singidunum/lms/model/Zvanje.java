package rs.ac.singidunum.lms.model;

import java.util.Date;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Zvanje {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	private Date datumIzbora;
	
	@Temporal(TemporalType.DATE)
	private Date datumPrestanka;
	
	@OneToOne(optional = false)
	private TipZvanja tipZvanja;
	
	@ManyToOne(optional= false)
	private NaucnaOblast naucnaOblast;
	
	@OneToMany(mappedBy = "zvanje")
	private Set<NastavniciZvanja> nastavnici = new HashSet<NastavniciZvanja>();
	

	public Zvanje() {
		super();
	}

	public Zvanje(Long id, Date datumIzbora, Date datumPrestanka, TipZvanja tipZvanja, NaucnaOblast naucnaOblast) {
		super();
		this.id = id;
		this.datumIzbora = datumIzbora;
		this.datumPrestanka = datumPrestanka;
		this.tipZvanja = tipZvanja;
		this.naucnaOblast = naucnaOblast;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}



	public Date getDatumIzbora() {
		return datumIzbora;
	}



	public void setDatumIzbora(Date datumIzbora) {
		this.datumIzbora = datumIzbora;
	}



	public Date getDatumPrestanka() {
		return datumPrestanka;
	}



	public void setDatumPrestanka(Date datumPrestanka) {
		this.datumPrestanka = datumPrestanka;
	}



	public TipZvanja getTipZvanja() {
		return tipZvanja;
	}



	public void setTipZvanja(TipZvanja tipZvanja) {
		this.tipZvanja = tipZvanja;
	}



	public NaucnaOblast getNaucnaOblast() {
		return naucnaOblast;
	}



	public void setNaucnaOblast(NaucnaOblast naucnaOblast) {
		this.naucnaOblast = naucnaOblast;
	}



	public Set<NastavniciZvanja> getNastavnici() {
		return this.nastavnici;
	}



	public void setNastavnici(Set<NastavniciZvanja> nastavnici) {
		this.nastavnici = nastavnici;
	}
	
}
