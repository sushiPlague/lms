package rs.ac.singidunum.lms.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.dto.RasporedPredavanjaDTO;
import rs.ac.singidunum.lms.model.RasporedPredavanja;
import rs.ac.singidunum.lms.service.RasporedPredavanjaService;

@Controller
@RequestMapping(path = "/rasporedi-predavanja")
public class RasporedPredavanjaController {
	
	@Autowired
	private RasporedPredavanjaService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<RasporedPredavanjaDTO>> getAll(){
		ArrayList<RasporedPredavanjaDTO> t = new ArrayList<RasporedPredavanjaDTO>();
		Iterable<RasporedPredavanja> t1 = service.findAll();
		for (RasporedPredavanja t2: t1) {
			t.add(t2.getDTO());
		}
		return new ResponseEntity<ArrayList<RasporedPredavanjaDTO>>(t, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<RasporedPredavanjaDTO> getOne(@PathVariable("id") Long id){
		Optional<RasporedPredavanja> t = service.findOne(id);
		if (t.isPresent()) {
			return new ResponseEntity<RasporedPredavanjaDTO>(t.get().getDTO(), HttpStatus.OK);
		}
		return new ResponseEntity<RasporedPredavanjaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<RasporedPredavanjaDTO> update(@RequestBody RasporedPredavanja t, @PathVariable("id") Long id, @RequestHeader("Authorization") String token){
		Optional<RasporedPredavanja> t1 = service.findOne(id);
		if (t1.isPresent()) {
			t.setId(id);
			service.save(t);
			return new ResponseEntity<RasporedPredavanjaDTO>(t.getDTO(), HttpStatus.CREATED);
		}
		return new ResponseEntity<RasporedPredavanjaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RasporedPredavanjaDTO> delete(@PathVariable("id") Long id, @RequestHeader("Authorization") String token){
		Optional<RasporedPredavanja> t1 = service.findOne(id);
		if (t1.isPresent()) {
			service.delete(id);
			return new ResponseEntity<RasporedPredavanjaDTO>(HttpStatus.OK);
		}
		return new ResponseEntity<RasporedPredavanjaDTO>(HttpStatus.BAD_REQUEST);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path="", method=RequestMethod.POST)
	public ResponseEntity<RasporedPredavanjaDTO> create(@RequestBody RasporedPredavanja t, @RequestHeader("Authorization") String token){
		try {
			service.save(t);
			return new ResponseEntity<RasporedPredavanjaDTO>(t.getDTO(), HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<RasporedPredavanjaDTO>(HttpStatus.BAD_REQUEST);
	}
}
