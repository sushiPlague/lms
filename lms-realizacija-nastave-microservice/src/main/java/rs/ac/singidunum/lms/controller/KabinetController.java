package rs.ac.singidunum.lms.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.dto.KabinetDTO;
import rs.ac.singidunum.lms.model.Kabinet;
import rs.ac.singidunum.lms.service.KabinetService;

@Controller
@RequestMapping(path = "/kabineti")
public class KabinetController {
	
	@Autowired
	private KabinetService service;
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<KabinetDTO>> getAll(){
		ArrayList<KabinetDTO> t = new ArrayList<KabinetDTO>();
		Iterable<Kabinet> t1 = service.findAll();
		for (Kabinet t2: t1) {
			t.add(t2.getDTO());
		}
		return new ResponseEntity<ArrayList<KabinetDTO>>(t, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KabinetDTO> getOne(@PathVariable("id") Long id){
		Optional<Kabinet> t = service.findOne(id);
		if (t.isPresent()) {
			return new ResponseEntity<KabinetDTO>(t.get().getDTO(), HttpStatus.OK);
		}
		return new ResponseEntity<KabinetDTO>(HttpStatus.NOT_FOUND);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<KabinetDTO> update(@RequestBody Kabinet t, @PathVariable("id") Long id, @RequestHeader("Authorization") String token){
		Optional<Kabinet> t1 = service.findOne(id);
		if (t1.isPresent() == false) {
			return new ResponseEntity<KabinetDTO>(HttpStatus.NOT_FOUND);
		} else {
			t.setId(id);
			service.save(t);
			return new ResponseEntity<KabinetDTO>(t.getDTO(), HttpStatus.CREATED);
		}
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<KabinetDTO> delete(@PathVariable("id") Long id, @RequestHeader("Authorization") String token){
		Optional<Kabinet> t1 = service.findOne(id);
		if (t1.isPresent()) {
			service.delete(id);
			return new ResponseEntity<KabinetDTO>(HttpStatus.OK);
		}
		return new ResponseEntity<KabinetDTO>(HttpStatus.BAD_REQUEST);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path="", method=RequestMethod.POST)
	public ResponseEntity<KabinetDTO> create(@RequestBody Kabinet t, @RequestHeader("Authorization") String token){
		try {
			service.save(t);
			return new ResponseEntity<KabinetDTO>(t.getDTO(), HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<KabinetDTO>(HttpStatus.BAD_REQUEST);
	}
}
