package rs.ac.singidunum.lms.dto;

public class KabinetDTO {
	private Long id;
	private String naziv;
	
	public KabinetDTO() {
		super();
	}
	
	public KabinetDTO(Long id, String naziv) {
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
}
