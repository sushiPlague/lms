package rs.ac.singidunum.lms.service;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;


public abstract class CrudService<T, ID extends Serializable> {
	@Autowired
	protected CrudRepository<T, ID> repository;
	
	public Iterable<T> findAll() {
		return repository.findAll();
	}
	public Optional<T> findOne(ID id) {
		return repository.findById(id);
	}
	public T save(T model) {
		return repository.save(model);
	}
	public void delete(ID id) {
		repository.deleteById(id);
	}
		
}