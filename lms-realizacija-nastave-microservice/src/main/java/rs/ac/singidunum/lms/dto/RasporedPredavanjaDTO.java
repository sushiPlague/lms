
package rs.ac.singidunum.lms.dto;

import java.util.Date;

public class RasporedPredavanjaDTO {
	private Long id;
	private Date vremePocetka;
	private Date vremeZavrsetka;
	private KabinetDTO kabinet;
	private NastavnikDTO nastavnik;
	private PredmetDTO predmet;
	
	public RasporedPredavanjaDTO() {
		super();
	}

	public RasporedPredavanjaDTO(Long id, Date vremePocetka, Date vremeZavrsetka, KabinetDTO kabinet, NastavnikDTO nastavnik, PredmetDTO predmet) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.kabinet = kabinet;
		this.nastavnik = nastavnik;
		this.predmet = predmet;
	}
	
	public RasporedPredavanjaDTO(Long id, Date vremePocetka, Date vremeZavrsetka) {
		this(id, vremePocetka, vremeZavrsetka, null, null, null);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(Date vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public Date getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(Date vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public KabinetDTO getKabinet() {
		return kabinet;
	}

	public void setKabinet(KabinetDTO kabinet) {
		this.kabinet = kabinet;
	}

	public NastavnikDTO getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(NastavnikDTO nastavnik) {
		this.nastavnik = nastavnik;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}
	
	
}
