package rs.ac.singidunum.lms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.model.Kabinet;

@Repository
public interface KabinetRepository extends CrudRepository<Kabinet, Long> {

}
