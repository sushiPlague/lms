package rs.ac.singidunum.lms.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Administrator;
import rs.ac.singidunum.lms.repository.AdministratorRepository;

@Service
public class AdministratorService extends CrudService<Administrator, Long> {
	public Optional<Administrator> findByKorisnickoIme(String korisnickoIme){
		return ((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme);
	}
	
	@Override
	public Administrator save(Administrator model) {
		String korisnickoIme = model.getKorisnickoIme();
		// provera da li se radi kreiranje ili izmena
		// kreiranje nema id
		if (model.getId() == null) {
			// znaci da bi se kreirao
			if(((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme).isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				return null;
			} else {
				// ne postoji korisnik sa tim korisnickim imenom
				return repository.save(model);
			}
		} else {
			// korisnik se menja
			Optional<Administrator> administrator = ((AdministratorRepository)this.repository).findByKorisnickoIme(korisnickoIme);
			if(administrator.isPresent() == true) {
				// korisnik sa tim korisnickim imenom postoji
				// dozvoljeno samo ako je to njegovo korisnickoIme
				if (administrator.get().getId() == model.getId()) {
					return repository.save(model);
				} else {
					// nije njegovo korisnicko ime
					return null;
				}
			} else {
				// korisnicko ime je jedinstveno
				return repository.save(model);
			}
		}
	}
}
