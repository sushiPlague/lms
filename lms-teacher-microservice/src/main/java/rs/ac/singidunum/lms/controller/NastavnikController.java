package rs.ac.singidunum.lms.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.model.Nastavnik;
import rs.ac.singidunum.lms.service.NastavnikService;
import rs.ac.singidunum.lms.dto.NastavnikDTO;

@Controller
//@RequestMapping(path = "/nastavnici")
public class NastavnikController {
	
	@Autowired
	private NastavnikService service;
	
	// DA LI TREBA DA NASTAVNIKE MOGU DA VIDE I DRUGI NASTAVNICI, STUDENTI ILI SAMO ADMINI
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<NastavnikDTO>> getAll(){
		ArrayList<NastavnikDTO> t = new ArrayList<NastavnikDTO>();
		Iterable<Nastavnik> t1 = service.findAll();
		for (Nastavnik t2: t1) {
			t.add(t2.getDTO());
		}
		return new ResponseEntity<ArrayList<NastavnikDTO>>(t, HttpStatus.OK);
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<NastavnikDTO> getOne(@PathVariable("id") Long id, @RequestHeader("Authorization") String token){
		Optional<Nastavnik> t = service.findOne(id);
		if (t.isPresent()) {
			// provera da nastavnik moze samo svoje podatke da gleda, DA LI TREBA
//			if(service.doUsernamesMatch(t.get().getKorisnickoIme(), token)) {
//				return new ResponseEntity<NastavnikDTO>(t.get().getDTO(), HttpStatus.OK);
//			};
			return new ResponseEntity<NastavnikDTO>(t.get().getDTO(), HttpStatus.OK);
			
		}
		return new ResponseEntity<NastavnikDTO>(HttpStatus.NOT_FOUND);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<NastavnikDTO> update(@RequestBody Nastavnik t, @PathVariable("id") Long id){
		Optional<Nastavnik> t1 = service.findOne(id);
		if (t1.isPresent()) {
			t.setId(id);
			Nastavnik nastavnik = service.save(t);
			if (nastavnik == null) {
				return new ResponseEntity<NastavnikDTO>(HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<NastavnikDTO>(t.getDTO(), HttpStatus.CREATED);
		}
		return new ResponseEntity<NastavnikDTO>(HttpStatus.NOT_FOUND);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<NastavnikDTO> delete(@PathVariable("id") Long id){
		Optional<Nastavnik> t1 = service.findOne(id);
		if (t1.isPresent()) {
			service.delete(id);
			return new ResponseEntity<NastavnikDTO>(HttpStatus.OK);
		}
		return new ResponseEntity<NastavnikDTO>(HttpStatus.BAD_REQUEST);
	}
	
	// KREIRANJE NASTAVNIKA SE RADI U AUTH REGISTER

}