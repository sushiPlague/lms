package rs.ac.singidunum.lms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.lms.model.NastavniciZvanja;

@Repository
public interface NastavniciZvanjaRepository extends CrudRepository<NastavniciZvanja, Long> {

}
