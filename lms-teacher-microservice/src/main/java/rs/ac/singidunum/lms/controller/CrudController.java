package rs.ac.singidunum.lms.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.dto.DTO;
import rs.ac.singidunum.lms.model.ModelEntity;
import rs.ac.singidunum.lms.service.CrudService;


//@RequestMapping(path = "/api/korisnici")
public abstract class CrudController<T extends ModelEntity<ID>, ID extends Serializable> {
	
	@Autowired
	protected CrudService<T, ID> service;
	
//	@Secured("ROLE_NASTAVNIK")
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<DTO>> getAll(){
		ArrayList<DTO> t = new ArrayList<DTO>();
		Iterable<T> t1 = service.findAll();
		for (T t2: t1) {
			t.add(t2.getDTO());
		}
		return new ResponseEntity<ArrayList<DTO>>(t, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<DTO> getOne(@PathVariable("id") ID id){
		Optional<T> t = service.findOne(id);
		if (t.isPresent()) {
			return new ResponseEntity<DTO>(t.get().getDTO(), HttpStatus.OK);
		}
		return new ResponseEntity<DTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path="", method=RequestMethod.POST)
	public ResponseEntity<DTO> create(@RequestBody T t){
		try {
			service.save(t);
			return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<DTO>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<DTO> update(@RequestBody T t, @PathVariable("id") ID id){
		Optional<T> t1 = service.findOne(id);
		if (t1.isPresent()) {
			t.setId(id);
			service.save(t);
			return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.CREATED);
		}
		return new ResponseEntity<DTO>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<DTO> delete(@PathVariable("id") ID id){
		Optional<T> t1 = service.findOne(id);
		if (t1.isPresent()) {
			service.delete(id);
			return new ResponseEntity<DTO>(HttpStatus.OK);
		}
		return new ResponseEntity<DTO>(HttpStatus.BAD_REQUEST);
	}
}