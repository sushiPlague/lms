package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.NaucnaOblast;

@Controller
@RequestMapping(path = "/naucne-oblasti")
public class NaucnaOblastController extends CrudController<NaucnaOblast, Long> {

}
