package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.NastavniciZvanja;

@Service
public class NastavniciZvanjaService extends CrudService<NastavniciZvanja, Long> {

}
