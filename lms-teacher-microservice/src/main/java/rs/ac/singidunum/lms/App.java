package rs.ac.singidunum.lms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
// prodiskutovati da li samo admini mogu da rade izmene nad zvanjima, naucnim oblastima i tip zvanja,
// da li svi mogu da vide podatke o profesorima ili da bude npr poseban dto spram uloge koja pristupa,
// da li nastavnici i admini mogu da menjaju svoje podatke ili to rade admini
// da li admin moze da menja drugog admina ili samo sebe