package rs.ac.singidunum.lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import rs.ac.singidunum.lms.dto.AdministratorDTO;

@Entity
public class Administrator implements ModelEntity<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String korisnickoIme;
	
	@Column(nullable = false)
	private String lozinka;

	public Administrator() {
		super();
	}

	public Administrator(Long id, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	
	@Override
	public AdministratorDTO getDTO() {
		return new AdministratorDTO(this.id, this.korisnickoIme, this.lozinka);
	}
	
	
}
