package rs.ac.singidunum.lms.dto;


public class NastavniciZvanjaDTO extends DTO {
	private Long id;
	private NastavnikDTO nastavnik;
	private ZvanjeDTO zvanje;
	
	public NastavniciZvanjaDTO() {
		super();
	}
	
	public NastavniciZvanjaDTO(Long id, NastavnikDTO nastavnik, ZvanjeDTO zvanje) {
		super();
		this.id = id;
		this.nastavnik = nastavnik;
		this.zvanje = zvanje;
	}
	
	public NastavniciZvanjaDTO(Long id, NastavnikDTO nastavnik) {
		this.id = id;
		this.nastavnik = nastavnik;
	}
	
	public NastavniciZvanjaDTO(Long id, ZvanjeDTO zvanje) {
		this.id = id;
		this.zvanje = zvanje;
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NastavnikDTO getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(NastavnikDTO nastavnik) {
		this.nastavnik = nastavnik;
	}

	public ZvanjeDTO getZvanje() {
		return zvanje;
	}

	public void setZvanje(ZvanjeDTO zvanje) {
		this.zvanje = zvanje;
	}
	
	
}
