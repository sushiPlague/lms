package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.NastavniciZvanja;

@Controller
@RequestMapping(path = "/nastavnici-zvanja")
public class NastavniciZvanjaController extends CrudController<NastavniciZvanja, Long> {
	
}
