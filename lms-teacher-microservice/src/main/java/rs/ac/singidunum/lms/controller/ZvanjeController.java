package rs.ac.singidunum.lms.controller;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.lms.dto.DTO;
import rs.ac.singidunum.lms.model.Zvanje;

@Controller
@RequestMapping(path = "/zvanja")
public class ZvanjeController extends CrudController<Zvanje, Long> {

	@Override
	@Secured("ROLE_ADMIN")
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<DTO>> getAll(){
		ArrayList<DTO> t = new ArrayList<DTO>();
		Iterable<Zvanje> t1 = service.findAll();
		for (Zvanje t2: t1) {
			t.add(t2.getDTO());
		}
		return new ResponseEntity<ArrayList<DTO>>(t, HttpStatus.OK);
	}
}
