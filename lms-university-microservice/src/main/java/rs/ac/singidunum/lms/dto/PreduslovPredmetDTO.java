package rs.ac.singidunum.lms.dto;

public class PreduslovPredmetDTO extends DTO {
	String naziv;
	PredmetDTO predmet;
	
	public PreduslovPredmetDTO(String naziv, PredmetDTO predmet) {
		super();
		this.naziv = naziv;
		this.predmet = predmet;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}
}
