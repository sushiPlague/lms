package rs.ac.singidunum.lms.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.lms.dto.GodinaStudijaDTO;

@Entity
public class GodinaStudija implements ModelEntity<Long> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	Date[] godina = new Date[2];
	
	@ManyToOne(optional = false)
	StudijskiProgram studijskiProgram;
	
	@OneToMany(mappedBy = "godinaStudija")
	Set<Predmet> predmeti = new HashSet<Predmet>();

	public GodinaStudija() {
		super();
	}

	public GodinaStudija(Long id, Date[] godina, StudijskiProgram studijskiProgram, Set<Predmet> predmeti) {
		super();
		this.id = id;
		this.godina = godina;
		this.studijskiProgram = studijskiProgram;
		this.predmeti = predmeti;
	}

	public Date[] getGodina() {
		return godina;
	}

	public void setGodina(Date[] godina) {
		this.godina = godina;
	}
	
	public void setGodina(Date prvaGodina, Date drugaGodina) {
		this.godina[0] = prvaGodina;
		this.godina[1] = drugaGodina;
	}

	public StudijskiProgram getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(StudijskiProgram studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}

	public Set<Predmet> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(Set<Predmet> predmeti) {
		this.predmeti = predmeti;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	@Override
	public GodinaStudijaDTO getDTO() {
		return new GodinaStudijaDTO(getGodina(), getStudijskiProgram().getDTO());
	}
	
}
