package rs.ac.singidunum.lms.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import rs.ac.singidunum.lms.dto.IshodDTO;
import rs.ac.singidunum.lms.dto.PredmetDTO;
import rs.ac.singidunum.lms.dto.PreduslovPredmetDTO;

@Entity
public class Predmet implements ModelEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(nullable = false)
	String naziv;
	
	@Column(nullable = false)
	int espb;
	
	@Column(nullable = false)
	boolean obavezan;
	
	@Column(nullable = false)
	int brojPredavanja;
	
	@Column(nullable = false)
	int brojVezbi;
	
	@Column(nullable = false)
	int drugiObliciNastave;
	
	@Column
	int istrazivackiRad;
	
	@Column(nullable = false)
	int ostaliCasovi;
	
	@OneToMany(mappedBy = "parentPredmet")
	Set<PreduslovPredmet> preduslovi = new HashSet<PreduslovPredmet>();
	
	@ManyToOne
	GodinaStudija godinaStudija;
	
	@OneToMany(mappedBy = "predmet")
	Set<Ishod> silabus = new HashSet<Ishod>();
	
	public Predmet() {
		super();
	}
	
	public Predmet(Long id, String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi,
			GodinaStudija godinaStudija, Set<PreduslovPredmet> predmeti, Set<Ishod> preduslovi) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.espb = espb;
		this.obavezan = obavezan;
		this.brojPredavanja = brojPredavanja;
		this.brojVezbi = brojVezbi;
		this.drugiObliciNastave = drugiObliciNastave;
		this.istrazivackiRad = istrazivackiRad;
		this.ostaliCasovi = ostaliCasovi;
		this.preduslovi = predmeti;
		this.godinaStudija = godinaStudija;
		this.silabus = preduslovi;
	}
	
	public Set<PreduslovPredmet> getPreduslovi() {
		return preduslovi;
	}

	public void setPreduslovi(Set<PreduslovPredmet> preduslovi) {
		this.preduslovi = preduslovi;
	}

	public Set<Ishod> getSilabus() {
		return silabus;
	}

	public void setSilabus(Set<Ishod> silabus) {
		this.silabus = silabus;
	}

	public GodinaStudija getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudija godinaStudija) {
		this.godinaStudija = godinaStudija;
	}


	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getEspb() {
		return espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean isObavezan() {
		return obavezan;
	}

	public void setObavezan(boolean obavezan) {
		this.obavezan = obavezan;
	}

	public int getBrojPredavanja() {
		return brojPredavanja;
	}

	public void setBrojPredavanja(int brojPredavanja) {
		this.brojPredavanja = brojPredavanja;
	}

	public int getBrojVezbi() {
		return brojVezbi;
	}

	public void setBrojVezbi(int brojVezbi) {
		this.brojVezbi = brojVezbi;
	}

	public int getDrugiObliciNastave() {
		return drugiObliciNastave;
	}

	public void setDrugiObliciNastave(int drugiObliciNastave) {
		this.drugiObliciNastave = drugiObliciNastave;
	}

	public int getIstrazivackiRad() {
		return istrazivackiRad;
	}

	public void setIstrazivackiRad(int istrazivackiRad) {
		this.istrazivackiRad = istrazivackiRad;
	}

	public int getOstaliCasovi() {
		return ostaliCasovi;
	}

	public void setOstaliCasovi(int ostaliCasovi) {
		this.ostaliCasovi = ostaliCasovi;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
		
	}
	
	@Override
	public PredmetDTO getDTO() {
		ArrayList<IshodDTO> silabusDTO = (ArrayList<IshodDTO>) this.silabus
				.stream().map(s -> new IshodDTO(s.getOpis(), s.getPredmet().getDTO()))
				.collect(Collectors.toList());
		
		ArrayList<PreduslovPredmetDTO> predusloviDTO = (ArrayList<PreduslovPredmetDTO>) this.preduslovi
				.stream().map(p -> new PreduslovPredmetDTO(p.getNaziv(), p.getParentPredmet().getDTO()))
				.collect(Collectors.toList());
		
		return new PredmetDTO(getNaziv(), getEspb(), isObavezan(), getBrojPredavanja(), getBrojVezbi(), getDrugiObliciNastave(), getIstrazivackiRad(), getOstaliCasovi(), getGodinaStudija().getDTO(), predusloviDTO, silabusDTO);
	}
	
}
