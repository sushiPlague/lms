package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Ishod;

@Controller
@RequestMapping(path = "/ishodi")
public class IshodController extends CrudController<Ishod, Long> {

}
