package rs.ac.singidunum.lms.dto;

import java.util.ArrayList;
import java.util.Date;

public class GodinaStudijaDTO extends DTO {
	Date[] godina = new Date[2];
	StudijskiProgramDTO studijskiProgram;
	ArrayList<PredmetDTO> predmeti;
	
	public GodinaStudijaDTO(Date[] godina, StudijskiProgramDTO studijskiProgram, ArrayList<PredmetDTO> predmeti) {
		super();
		this.godina = godina;
		this.studijskiProgram = studijskiProgram;
		this.predmeti = predmeti;
	}

	public GodinaStudijaDTO(Date[] godina, StudijskiProgramDTO studijskiProgram) {
		this(godina, studijskiProgram, new ArrayList<PredmetDTO>());
	}

	public Date[] getGodina() {
		return godina;
	}

	public void setGodina(Date[] godina) {
		this.godina = godina;
	}

	public StudijskiProgramDTO getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(StudijskiProgramDTO studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}

	public ArrayList<PredmetDTO> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(ArrayList<PredmetDTO> predmeti) {
		this.predmeti = predmeti;
	}
	
}
