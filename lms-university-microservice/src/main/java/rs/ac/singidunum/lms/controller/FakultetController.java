package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Fakultet;

@Controller
@RequestMapping(path = "/fakulteti")
public class FakultetController extends CrudController<Fakultet, Long> {

}
