package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.Predmet;

@Service
public class PredmetService extends CrudService<Predmet, Long> {

}
