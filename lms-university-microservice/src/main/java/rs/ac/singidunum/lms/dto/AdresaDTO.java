package rs.ac.singidunum.lms.dto;

public class AdresaDTO extends DTO {

	private Long id;
	private String ulica;
	private String broj;
	private MestoDTO mesto;
	private FakultetDTO fakultet;
	private UniverzitetDTO univerzitet;

	public AdresaDTO() {
		super();
	}

	public AdresaDTO(Long id, String ulica, String broj) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
	}

	public AdresaDTO(Long id, String ulica, String broj, MestoDTO mesto, FakultetDTO fakultet, UniverzitetDTO univerzitet) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
		this.fakultet = fakultet;
		this.univerzitet = univerzitet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public MestoDTO getMesto() {
		return mesto;
	}

	public void setMesto(MestoDTO mesto) {
		this.mesto = mesto;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}
	
}
