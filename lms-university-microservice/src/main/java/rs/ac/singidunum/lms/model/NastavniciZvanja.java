package rs.ac.singidunum.lms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import rs.ac.singidunum.lms.dto.NastavniciZvanjaDTO;
import rs.ac.singidunum.lms.dto.NastavnikDTO;
import rs.ac.singidunum.lms.dto.ZvanjeDTO;

@Entity
public class NastavniciZvanja implements ModelEntity<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	private Nastavnik nastavnik;
	
	@ManyToOne(optional = false)
	private Zvanje zvanje;

	public NastavniciZvanja() {
		super();
	}

	public NastavniciZvanja(Long id, Nastavnik nastavnik, Zvanje zvanje) {
		super();
		this.id = id;
		this.nastavnik = nastavnik;
		this.zvanje = zvanje;
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public Zvanje getZvanje() {
		return zvanje;
	}

	public void setZvanje(Zvanje zvanje) {
		this.zvanje = zvanje;
	}
	
	@Override
	public NastavniciZvanjaDTO getDTO() {
		NastavnikDTO nastavnik = new NastavnikDTO(this.nastavnik.getId(), this.nastavnik.getIme(), this.nastavnik.getPrezime(), this.nastavnik.getKorisnickoIme(), this.nastavnik.getLozinka(), this.nastavnik.getBiografija(), this.nastavnik.getJmbg());
		ZvanjeDTO zvanje = new ZvanjeDTO(this.zvanje.getId(), this.zvanje.getDatumIzbora(), this.zvanje.getDatumPrestanka());
		
		return new NastavniciZvanjaDTO(this.id, nastavnik, zvanje);
	}
}
