package rs.ac.singidunum.lms.dto;

import java.util.ArrayList;

public class StudijskiProgramDTO extends DTO {
	String naziv;
	NastavnikDTO rukovodilac;
	FakultetDTO fakultet;
	ArrayList<GodinaStudijaDTO> godineStudija;
	
	public StudijskiProgramDTO(String naziv, NastavnikDTO rukovodilac, FakultetDTO fakultet,
			ArrayList<GodinaStudijaDTO> godineStudija) {
		super();
		this.naziv = naziv;
		this.rukovodilac = rukovodilac;
		this.fakultet = fakultet;
		this.godineStudija = godineStudija;
	}

	public StudijskiProgramDTO(String naziv, NastavnikDTO rukovodilac, FakultetDTO fakultet) {
		this(naziv, rukovodilac, fakultet, new ArrayList<GodinaStudijaDTO>());
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public NastavnikDTO getRukovodilac() {
		return rukovodilac;
	}

	public void setRukovodilac(NastavnikDTO rukovodilac) {
		this.rukovodilac = rukovodilac;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}

	public ArrayList<GodinaStudijaDTO> getGodineStudija() {
		return godineStudija;
	}

	public void setGodineStudija(ArrayList<GodinaStudijaDTO> godineStudija) {
		this.godineStudija = godineStudija;
	}
	
}
