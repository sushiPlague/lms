package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Predmet;

@Controller
@RequestMapping(path = "/predmeti")
public class PredmetController extends CrudController<Predmet, Long> {

}
