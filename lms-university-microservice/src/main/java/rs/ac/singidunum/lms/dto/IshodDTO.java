package rs.ac.singidunum.lms.dto;

public class IshodDTO extends DTO {
	String opis;
	PredmetDTO predmet;
	
	public IshodDTO(String opis, PredmetDTO predmet) {
		super();
		this.opis = opis;
		this.predmet = predmet;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}
}
