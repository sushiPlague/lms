package rs.ac.singidunum.lms.dto;

import java.util.ArrayList;

public class PredmetDTO extends DTO {
	String naziv;
	int espb;
	boolean obavezan;
	int brojPredavanja;
	int brojVezbi;
	int drugiObliciNastave;
	int istrazivackiRad;
	int ostaliCasovi;
	GodinaStudijaDTO godinaStudija;
	ArrayList<PreduslovPredmetDTO> preduslovi;
	ArrayList<IshodDTO> silabus;


	public PredmetDTO(String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi, GodinaStudijaDTO godinaStudija,
			ArrayList<PreduslovPredmetDTO> preduslovi, ArrayList<IshodDTO> silabus) {
		super();
		this.naziv = naziv;
		this.espb = espb;
		this.obavezan = obavezan;
		this.brojPredavanja = brojPredavanja;
		this.brojVezbi = brojVezbi;
		this.drugiObliciNastave = drugiObliciNastave;
		this.istrazivackiRad = istrazivackiRad;
		this.ostaliCasovi = ostaliCasovi;
		this.godinaStudija = godinaStudija;
		this.preduslovi = preduslovi;
		this.silabus = silabus;
	}

	public PredmetDTO(String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi,
			GodinaStudijaDTO godinaStudija) {
		this(naziv, espb, obavezan, brojPredavanja, brojVezbi, drugiObliciNastave, istrazivackiRad, ostaliCasovi, godinaStudija, new ArrayList<PreduslovPredmetDTO>(), new ArrayList<IshodDTO>());
	}
	
	public ArrayList<PreduslovPredmetDTO> getPreduslovi() {
		return preduslovi;
	}

	public void setPreduslovi(ArrayList<PreduslovPredmetDTO> preduslovi) {
		this.preduslovi = preduslovi;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getEspb() {
		return espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean isObavezan() {
		return obavezan;
	}

	public void setObavezan(boolean obavezan) {
		this.obavezan = obavezan;
	}

	public int getBrojPredavanja() {
		return brojPredavanja;
	}

	public void setBrojPredavanja(int brojPredavanja) {
		this.brojPredavanja = brojPredavanja;
	}

	public int getBrojVezbi() {
		return brojVezbi;
	}

	public void setBrojVezbi(int brojVezbi) {
		this.brojVezbi = brojVezbi;
	}

	public int getDrugiObliciNastave() {
		return drugiObliciNastave;
	}

	public void setDrugiObliciNastave(int drugiObliciNastave) {
		this.drugiObliciNastave = drugiObliciNastave;
	}

	public int getIstrazivackiRad() {
		return istrazivackiRad;
	}

	public void setIstrazivackiRad(int istrazivackiRad) {
		this.istrazivackiRad = istrazivackiRad;
	}

	public int getOstaliCasovi() {
		return ostaliCasovi;
	}

	public void setOstaliCasovi(int ostaliCasovi) {
		this.ostaliCasovi = ostaliCasovi;
	}

	public GodinaStudijaDTO getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudijaDTO godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public ArrayList<IshodDTO> getSilabus() {
		return silabus;
	}

	public void setSilabus(ArrayList<IshodDTO> silabus) {
		this.silabus = silabus;
	}
	
}
