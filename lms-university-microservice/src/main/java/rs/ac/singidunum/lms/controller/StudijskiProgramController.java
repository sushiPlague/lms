package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.StudijskiProgram;

@Controller
@RequestMapping(path = "/studijski-programi")
public class StudijskiProgramController extends CrudController<StudijskiProgram, Long> {

}
