package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.GodinaStudija;

@Controller
@RequestMapping(path = "/godine-studija")
public class GodinaStudijaController extends CrudController<GodinaStudija, Long> {

}
