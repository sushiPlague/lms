package rs.ac.singidunum.lms.model;

import java.io.Serializable;

import rs.ac.singidunum.lms.dto.DTO;

public interface ModelEntity<ID extends Serializable> {
	// mozda cemo kod update-a zahtevati id, a za upis je zahtevano da id bude serializable
	public ID getId();
	public void setId(ID id);
	// trebace nam dto objekat od nekog objekta koji budemo zeleli da vratimo
	// kako bi kod u controlleru bio nezavisan i moguc za genericke tipove, treba detaljniju implementaciju kreiranja dto objekta 
	// staviti u objekat koji ce kreirati svoj dto
	public DTO getDTO();
}