package rs.ac.singidunum.lms.dto;

import java.util.ArrayList;

public class FakultetDTO extends DTO {
	String naziv;
	NastavnikDTO dekan;
	UniverzitetDTO univerzitet;
	ArrayList<StudijskiProgramDTO> studijskiProgrami;
	AdresaDTO adresa;
	
	public FakultetDTO() {
		super();
	}

	public FakultetDTO(String naziv, NastavnikDTO dekan, UniverzitetDTO univerzitet, AdresaDTO adresa,
			ArrayList<StudijskiProgramDTO> studijskiProgrami) {
		super();
		this.naziv = naziv;
		this.dekan = dekan;
		this.univerzitet = univerzitet;
		this.studijskiProgrami = studijskiProgrami;
		this.adresa = adresa;
	}

	public FakultetDTO(String naziv, NastavnikDTO dekan, UniverzitetDTO univerzitet, AdresaDTO adresa) {
		this(naziv, dekan, univerzitet, adresa, new ArrayList<StudijskiProgramDTO>());
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public NastavnikDTO getDekan() {
		return dekan;
	}
	public void setDekan(NastavnikDTO dekan) {
		this.dekan = dekan;
	}
	
	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public ArrayList<StudijskiProgramDTO> getStudijskiProgrami() {
		return studijskiProgrami;
	}
	public void setStudijskiProgrami(ArrayList<StudijskiProgramDTO> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}
}
