package rs.ac.singidunum.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.lms.model.Univerzitet;

@Controller
@RequestMapping(path = "/univerziteti")
public class UniverzitetController extends CrudController<Univerzitet, Long> {

}
