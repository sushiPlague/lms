package rs.ac.singidunum.lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import rs.ac.singidunum.lms.dto.DrzavaDTO;
import rs.ac.singidunum.lms.dto.MestoDTO;

@Entity
public class Mesto implements ModelEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	@Column(nullable = false)
	private String naziv;

	@ManyToOne(optional = false)
	private Drzava drzava;

	public Mesto() {
		super();
	}

	public Mesto(Long id, String naziv, Drzava drzava) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.drzava = drzava;
	}

	public Drzava getDrzava() {
		return drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public MestoDTO getDTO() {
		return new MestoDTO(this.id, this.naziv, new DrzavaDTO(this.getDrzava().getId(), this.getDrzava().getNaziv()));
	}

}
