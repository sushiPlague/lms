package rs.ac.singidunum.lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import rs.ac.singidunum.lms.dto.DTO;
import rs.ac.singidunum.lms.dto.PreduslovPredmetDTO;

@Entity
public class PreduslovPredmet implements ModelEntity<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(nullable = false)
	String naziv;
	
	@ManyToOne
	Predmet parentPredmet;
	
	public PreduslovPredmet(Long id, String naziv, Predmet parentPredmet) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.parentPredmet = parentPredmet;
	}
	
	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Predmet getParentPredmet() {
		return parentPredmet;
	}

	public void setParentPredmet(Predmet parentPredmet) {
		this.parentPredmet = parentPredmet;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	@Override
	public DTO getDTO() {
		return new PreduslovPredmetDTO(this.naziv, parentPredmet.getDTO());
	}
}
