package rs.ac.singidunum.lms.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.lms.model.GodinaStudija;

@Service
public class GodinaStudijaService extends CrudService<GodinaStudija, Long> {

}
