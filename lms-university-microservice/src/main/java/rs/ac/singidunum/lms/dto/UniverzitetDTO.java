package rs.ac.singidunum.lms.dto;

import java.util.ArrayList;
import java.util.Date;

public class UniverzitetDTO extends DTO {
	String naziv;
	Date datumOsnivanja;
	NastavnikDTO rektor;
	ArrayList<FakultetDTO> fakulteti;
	AdresaDTO adresa;
	
	public UniverzitetDTO() {
		super();
	}

	public UniverzitetDTO(String naziv, Date datumOsnivanja, NastavnikDTO rektor, AdresaDTO adresa, ArrayList<FakultetDTO> fakulteti) {
		super();
		this.naziv = naziv;
		this.datumOsnivanja = datumOsnivanja;
		this.rektor = rektor;
		this.fakulteti = fakulteti;
		this.adresa = adresa;
	}

	public UniverzitetDTO(String naziv, Date datumOsnivanja, NastavnikDTO rektor, AdresaDTO adresa) {
		this(naziv, datumOsnivanja, rektor, adresa, new ArrayList<FakultetDTO>());
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Date getDatumOsnivanja() {
		return datumOsnivanja;
	}

	public void setDatumOsnivanja(Date datumOsnivanja) {
		this.datumOsnivanja = datumOsnivanja;
	}
	
	public NastavnikDTO getRektor() {
		return rektor;
	}

	public void setRektor(NastavnikDTO rektor) {
		this.rektor = rektor;
	}

	public ArrayList<FakultetDTO> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(ArrayList<FakultetDTO> fakulteti) {
		this.fakulteti = fakulteti;
	}
	
}
