package rs.ac.singidunum.lms.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.lms.dto.FakultetDTO;
import rs.ac.singidunum.lms.dto.NastavnikDTO;
import rs.ac.singidunum.lms.dto.UniverzitetDTO;

@Entity
public class Univerzitet implements ModelEntity<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(nullable = false)
	String naziv;
	
	@Temporal(TemporalType.TIMESTAMP)
	Date datumOsnivanja;
	
	@OneToMany(mappedBy = "univerzitet")
	Set<Fakultet> fakulteti = new HashSet<Fakultet>();
	
	@OneToOne(optional = false)
	Nastavnik rektor;
	
	@OneToOne(optional = false)
	Adresa adresa;
	
	public Univerzitet() {
		super();
	}

	public Univerzitet(Long id, String naziv, Date datumOsnivanja, Set<Fakultet> fakulteti, Nastavnik rektor,
			Adresa adresa) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.datumOsnivanja = datumOsnivanja;
		this.fakulteti = fakulteti;
		this.rektor = rektor;
		this.adresa = adresa;
	}
	
	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public Nastavnik getRektor() {
		return rektor;
	}

	public void setRektor(Nastavnik rektor) {
		this.rektor = rektor;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Date getDatumOsnivanja() {
		return datumOsnivanja;
	}

	public void setDatumOsnivanja(Date datumOsnivanja) {
		this.datumOsnivanja = datumOsnivanja;
	}

	public Set<Fakultet> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(Set<Fakultet> fakulteti) {
		this.fakulteti = fakulteti;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	@Override
	public UniverzitetDTO getDTO() {
		NastavnikDTO rektorDTO = rektor.getDTO();
		ArrayList<FakultetDTO> fakulteti = (ArrayList<FakultetDTO>) this.fakulteti
				.stream().map(f -> new FakultetDTO(f.getNaziv(), f.getDekan().getDTO(), f.getUniverzitet().getDTO(), f.getAdresa().getDTO()))
				.collect(Collectors.toList());
		return new UniverzitetDTO(this.naziv, this.datumOsnivanja, rektorDTO, this.adresa.getDTO(), fakulteti);
	}
}
