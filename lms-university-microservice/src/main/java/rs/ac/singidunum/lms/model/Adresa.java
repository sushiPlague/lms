package rs.ac.singidunum.lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import rs.ac.singidunum.lms.dto.AdresaDTO;
import rs.ac.singidunum.lms.dto.DrzavaDTO;
import rs.ac.singidunum.lms.dto.MestoDTO;

@Entity
public class Adresa implements ModelEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	@Column(nullable = false)
	private String ulica;

	@Column(nullable = false)
	private String broj;

	@ManyToOne(optional = false)
	private Mesto mesto;

	@OneToOne(mappedBy = "adresa")
	private Fakultet fakultet;
	
	@OneToOne(mappedBy = "adresa")
	private Univerzitet univerzitet;
	
	public Adresa() {
		super();
	}

	public Adresa(Long id, String ulica, String broj, Mesto mesto) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public Mesto getMesto() {
		return mesto;
	}

	public void setMesto(Mesto mesto) {
		this.mesto = mesto;
	}

	@Override
	public AdresaDTO getDTO() {
		return new AdresaDTO(this.id, this.ulica, this.broj,
				new MestoDTO(this.getMesto().getId(), this.getMesto().getNaziv(),
						new DrzavaDTO(this.getMesto().getDrzava().getId(), this.getMesto().getDrzava().getNaziv())),
							fakultet.getDTO(),
								univerzitet.getDTO());
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}
