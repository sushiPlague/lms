package rs.ac.singidunum.lms.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.lms.dto.FakultetDTO;
import rs.ac.singidunum.lms.dto.StudijskiProgramDTO;

@Entity
public class Fakultet implements ModelEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(nullable = false)
	String naziv;
	
	@OneToMany(mappedBy = "fakultet")
	Set<StudijskiProgram> studijskiProgrami = new HashSet<StudijskiProgram>();
	
	@ManyToOne(optional= false)
	Univerzitet univerzitet;
	
	@OneToOne(optional = false)
	Nastavnik dekan;
	
	@OneToOne(optional = false)
	Adresa adresa;
	
	public Fakultet() {
		super();
	}

	public Fakultet(Long id, String naziv, Set<StudijskiProgram> studijskiProgrami, Univerzitet univerzitet,
			Nastavnik dekan, Adresa adresa) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.studijskiProgrami = studijskiProgrami;
		this.univerzitet = univerzitet;
		this.dekan = dekan;
		this.adresa = adresa;
	}

	
	
	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public Nastavnik getDekan() {
		return dekan;
	}

	public void setDekan(Nastavnik dekan) {
		this.dekan = dekan;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<StudijskiProgram> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(Set<StudijskiProgram> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	@Override
	public FakultetDTO getDTO() {
		ArrayList<StudijskiProgramDTO> studijskiProgramiDTO = (ArrayList<StudijskiProgramDTO>) this.studijskiProgrami
				.stream().map(s -> new StudijskiProgramDTO(s.getNaziv(), s.getRukovodilac().getDTO(), s.getFakultet().getDTO()))
				.collect(Collectors.toList());
		return new FakultetDTO(getNaziv(), getDekan().getDTO(), getUniverzitet().getDTO(), getAdresa().getDTO(), studijskiProgramiDTO);
	}

}
