package rs.ac.singidunum.lms.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.ac.singidunum.lms.dto.StudijskiProgramDTO;

@Entity
public class StudijskiProgram implements ModelEntity<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	String naziv;
	
	@ManyToOne(optional = false)
	Fakultet fakultet;
	
	@OneToMany(mappedBy = "studijskiProgram")
	Set<GodinaStudija> godineStudija = new HashSet<GodinaStudija>();
	
	@OneToOne(optional = false)
	Nastavnik rukovodilac;

	public StudijskiProgram() {
		super();
	}
	
	public StudijskiProgram(Long id, String naziv, Fakultet fakultet, Set<GodinaStudija> godineStudija,
			Nastavnik rukovodilac) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.fakultet = fakultet;
		this.godineStudija = godineStudija;
		this.rukovodilac = rukovodilac;
	}

	public Nastavnik getRukovodilac() {
		return rukovodilac;
	}

	public void setRukovodilac(Nastavnik rukovodilac) {
		this.rukovodilac = rukovodilac;
	}

	public Set<GodinaStudija> getGodineStudija() {
		return godineStudija;
	}

	public void setGodineStudija(Set<GodinaStudija> godineStudija) {
		this.godineStudija = godineStudija;
	}

	@Override
	public Long getId() {
		return id;
	}

	public Fakultet getFakultet() {
		return fakultet;
	}

	public void setFakultet(Fakultet fakultet) {
		this.fakultet = fakultet;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public StudijskiProgramDTO getDTO() {
		return new StudijskiProgramDTO(getNaziv(), getRukovodilac().getDTO(), getFakultet().getDTO());
	}
	
}
