package rs.ac.singidunum.lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import rs.ac.singidunum.lms.dto.IshodDTO;

@Entity
public class Ishod implements ModelEntity<Long> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column
	String opis;
	
	@ManyToOne
	Predmet predmet;

	public Ishod() {
		super();
	}

	public Ishod(Long id, String opis, Predmet predmet) {
		super();
		this.id = id;
		this.opis = opis;
		this.predmet = predmet;
	}

	public Predmet getPredmet() {
		return predmet;
	}


	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}


	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public IshodDTO getDTO() {
		return new IshodDTO(getOpis(), getPredmet().getDTO());
	}
}
